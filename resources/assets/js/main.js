//= ./partials/cube.portfolio.js
//= ./partials/animatescroll.js
//= ./partials/parallax.js
//= ./partials/parallax.min.js
//= ./partials/owl.carousel.js

'use strict';

(function ($) {
  var routesApp = {
    // All pages
    common: {
      init: function () {
      }
    },
    // Home page
    home: {
      init: function () {
      // JavaScript to be fired on the home page
      var itensMenu = $('ul.main-menu > li > a');
         itensMenu.each(function(i){
             $(this).on('click', function(e) {
                 e.preventDefault();
                 var goToSection = $( $(this).attr('href') );
                 itensMenu.removeClass('active');
                 if(goToSection.attr('id') === 'home'){
                     $('body').animatescroll();
                 } else {
                     $(this).addClass('active');
                     goToSection.animatescroll();
                 }
             });
         });

         $('.owl-carousel').owlCarousel({
              loop:true,
              nav:true,
              dots: false,
              navText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:1
                  },
                  1000:{
                      items:1
                  }
              }
          })
        //scroll-top
          $(function() {
            $("#scroll-top").on('click', function() {
              $("HTML, BODY").animate({
                  scrollTop: 0
              }, 1000);
            });
          });
          //mouse
          $(function() {
            $("#scroll-clientes").on('click', function() {
              $("HTML, BODY").animate({
                  scrollTop: 300
              }, 1000);
            });
          });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var namespace = routesApp;
      funcname = (funcname === undefined) ? 'init' : funcname;
      if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      UTIL.fire('common');

      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
      });
    }
  };

  $(document).ready(UTIL.loadEvents);
})(jQuery);
