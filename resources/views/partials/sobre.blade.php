<div id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="explain-video">
                    <img src="{{ App\getImage('fundovideo.png')}}" alt="" />
                    <a href="https://www.youtube.com/watch?v=-riS088QXX8" class="popup-video">
                        <i class="fa fa-play"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="about-bottom-left pt-80">
                    <h1>SOMOS UMA <span>AGÊNCIA DIGITAL</span> EM PROL DE CRIAR <span>GRANDES PRODUTOS</span> PARA <span>WEB</span> E <span>MÍDIA DIGITAL.</span></h1>
                    <p>Trabalhamos sempre visando garantir que sua ideia seja construída de forma inteligente para que você obtenha os melhores resultados!</p>
                    <br>
                    <p>Mas, só isso não basta.</p>
                    <br>
                    <p>Nos diferenciamos da concorrência porque adoramos o que fazemos e executamos cada novo trabalho com paixão. Não somos uma agência convencional. Por isso, somos a agência ideal para cuidar de seus projetos.</p>
                    <br>
                    <br>
                    <a id="scroll-clientes" href="#" class="button">NOSSOS CLIENTES</a>
                </div>
            </div>
        </div>
    </div>
</div>
