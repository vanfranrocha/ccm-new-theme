<div id="clients" class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="titleccm">
                    <h1><span>QUEM AMA</span> NOSSOS TRABALHOS</h1>
                    <p class="subtitleccm">Algumas das marcas e empresas que conquistamos a confiança através de relacionamento somado a um trabalho de qualidade.</p>
                </div>
                <?php
                    $images = get_field('clientes', 'option');
                ?>
                <div class="topclientes">
                    <?php foreach( $images as $image  ): ?>
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="imgclients">
                                        {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                        <a href="<?php echo $image['url']; ?>">
                                            <img class="imgclientes" src="<?php echo $image['url']; ?>"/>
                                        </a>
                                    </div>
                                </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
