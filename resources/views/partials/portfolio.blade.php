<div id="portfolio">
    <ul>
        <?php
               $args = array( 'post_type' => 'portfolio', 'posts_per_page' => 30, 'order' => 'ASC');
               query_posts( $args );
               while( have_posts() ) : the_post();
         ?>

        <div class="imagensport">
            <?php the_post_thumbnail('', array('class' => 'img-portfolio')); ?>
            <div class="textoportfolio">
                <h1>{{the_title()}}</h1>
                <p>{{the_excerpt()}}</p>
            </div>
            <div class="bgportfolio"></div>
        </div>
        <?php endwhile; ?>
    </ul>
</div>
