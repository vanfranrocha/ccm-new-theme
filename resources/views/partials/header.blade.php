<header id="home" class="header-top">
    <div class="top" data-parallax="scroll">
        <div class="container">
            <div class="col-md-12 col-sm-5 col-xs-8 logotop">
                <a href="/">
                    <img src="{{ App\getImage('logo.png')}}" alt="Logo">
                </a>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-9 texttop">
                <h1>VAMOS AJUDAR A TIRAR<br><span>SUA IDEIA</span> DO PAPEL!</h1>
                <h3>Ajudamos você a melhorar com <span>um bom design</span></h3>
            </div>
            <div id="mouse" class="col-md-4 col-sm-5 col-xs-8 mouse">
                <a href="#" onclick="$('[about]').animatescroll();">
                    <img src="{{ App\getImage('mouse.png')}}" alt="Mouse">
                </a>
            </div>
        </div>
    </div>
    @include('partials.nav-menu')
</header>
