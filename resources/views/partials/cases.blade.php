<div id="cases" class="cases">
    <div class="container">
        <div class="row">
            <div class="titlecases">
                <h1>CASES DE <span>SUCESSO</span></h1>
                <p class="subtitleccm">Conheça os nossos cases de sucesso marcantes que através do nosso trabalho redesenharam sua forma de alcançar seus clientes </p>
            </div>

             <div class="owl-carousel owl-theme">
                 <?php
                        $args = array( 'post_type' => 'cases', 'posts_per_page' => 10, 'order' => 'ASC');
                        query_posts( $args );
                        while( have_posts() ) : the_post();
                  ?>
                  <div class="item">
                      <div class="col-md-12 col-sm-12 col-xs-12 boxcases">
                         <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="img-cases">
                                <?php the_post_thumbnail('services-thumb', array('class' => 'img-cases')); ?>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12">
                           <div class="col-md-12 col-sm-12 col-xs-12 boxcasestext">
                                <h1><span>{{the_title()}}</span></h1>
                                <p>{{the_excerpt()}}</p>
                                <div class="info">
                                    <img src="<?php the_field('imagem_empresa'); ?>"/>
                                    <p class="infosubtext"><?php the_field('empresa'); ?></p>
                                    <p class="infosubfuncao"><?php the_field('funcao'); ?></p>
                                </div>
                                <div class="col-md-5 buttoninfo">
                                    <a href="#" class="buttoncase" onclick="$('[#about]').animatescroll();">VEJA SOBRE</a>
                                </div>
                                <br>
                           </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>

        </div>
    </div>
</div>
