<div id="time" class="time">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="titleccm">
                    <h1>SOMOS UM <span>TIME PEQUENO</span> COM <span>GRANDES IDEIAS</span></h1>
                    <p class="subtitleccm">Mais do que pessoas com conhecimento técnico, somos uma família que ama o que faz.</p>
                </div>

                <div class="boxtime pt-80">
                    <?php
                           $args = array( 'post_type' => 'time', 'posts_per_page' => 10, 'order' => 'ASC');
                           query_posts( $args );
                           while( have_posts() ) : the_post();
                     ?>
                        <div class="col-md-4 col-sm-6 col-xs-12 imgtime">
                                {{the_post_thumbnail('time-thumb')}}
                                <div  class="texttime">
                                    <h2>{{the_title()}}</h2>
                                    <p>{{the_excerpt()}}</p>
                                </div>
                        </div>
                    <?php endwhile; ?>
                </div>

            </div>
        </div>
    </div>

</div>
