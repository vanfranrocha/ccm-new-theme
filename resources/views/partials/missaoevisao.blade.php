<div id="vision" class="vision">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="titleccm">
                    <h1><span>MISSÃO</span> E <span>VISÃO</span> NESSE MUNDO</h1>
                    <p class="subtitleccm">Nosso propósito é na geração de valores em soluções que atinjam pessoas reais e que possam transformar suas vidas.</p>
                </div>
                <div class="boxvision">
                        <div class="col-md-6 col-sm-6 col-xs-12 textvision">
                            <div class="textmissao">
                                {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                <h3><span>Missão</span></h3>
                                <p>Transformar a visão do mundo sobre desenvolvimento e design de produtos web, na construção de valores proporcionando mais felicidade às pessoas com produtos que funcionem perfeitamente.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 textvision">
                            <div class="textvisao">
                                <h3><span>Visão</span></h3>
                                <p>Um mundo onde tenhamos produtos e/ou serviços que ajudem as pessoas a terem uma vida mais simples.</p>
                            </div>
                        </div>
                </div>
                <div class="infovision">
                    <h2>Há mais de 6 anos trabalhando com empresas do Brasil e Exterior, somos referência em marketing digital no Tocantins.</h2>
                </div>
            </div>
        </div>
    </div>

</div>
