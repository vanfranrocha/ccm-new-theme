<footer class="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 boxfooter pt-80">
                <p>E Agora?</p>
                <h1>TEM ALGUMA IDEIA?</h1>
                <p>Legal! Então vamos conversar.</p>
            </div>
            <div class="col-md-6 col-xs-12">
                <img class="img-responsive" src="{{ App\getImage('ccm.png')}}" alt="ccm">
            </div>
            <div class="col-md-12 col-xs-12 endereco">
                <p>&copy; <?php echo date("Y"); ?> Agência CCM,  Av. José de Brito nº 807 - Sala 3         -         (63) 99988-7766         -         contato@agenciaccm.com.br</p>
            </div>
        </div>
    </div>
</footer>
