<div id="how" class="how">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="titleccm pt-80">
                <h1>FAZEMOS O QUE <span>AMAMOS</span>, SEMPRE!</h1>
                <p class="subtitleccm">Buscamos sempre produzir ideias que impactam positivamente com o seu meio.</p>
            </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                                <!-- Carousel Slides / Quotes -->
                                <div class="carousel-inner text-center">
                                    <!-- Quote 1 -->
                                    <div class="item active">
                                            <a>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80">
                                                    <div class="boxhowcircle">
                                                        {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                                        <img class="img-responsive " src="{{ App\getImage('desenvolvimento.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80 boxhowtext">
                                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                                        <h2>Desenvolvimento</h2>
                                                            <p>Somos apaixonados pelo que fazemos e nos preocupamos com o seu negócio e os resultados esperados por você. Somos uma pequena equipe com foco em grandes resultados, buscando sempre a melhoria contínua de nossos serviços impactando assim na melhoria de seu negócio.</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div>
                                    <!-- Quote 2 -->
                                    <div class="item">
                                            <a>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80">
                                                    <div class="boxhowcircle">
                                                        {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                                        <img class="img-responsive " src="{{ App\getImage('services2.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80 boxhowtext">
                                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                                        <h2>Design</h2>
                                                            <p>Somos apaixonados pelo que fazemos e nos preocupamos com o seu negócio e os resultados esperados por você. Somos uma pequena equipe com foco em grandes resultados, buscando sempre a melhoria contínua de nossos serviços impactando assim na melhoria de seu negócio.</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div>
                                    <!-- Quote 3 -->
                                    <div class="item">
                                            <a>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80">
                                                    <div class="boxhowcircle">
                                                        {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                                        <img class="img-responsive " src="{{ App\getImage('services3.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80 boxhowtext">
                                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                                        <h2>Apps</h2>
                                                            <p>Somos apaixonados pelo que fazemos e nos preocupamos com o seu negócio e os resultados esperados por você. Somos uma pequena equipe com foco em grandes resultados, buscando sempre a melhoria contínua de nossos serviços impactando assim na melhoria de seu negócio.</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div>
                                    <!-- Quote 4 -->
                                    <div class="item">
                                            <a>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80">
                                                    <div class="boxhowcircle">
                                                        {{-- {{the_post_thumbnail('services-thumb')}} --}}
                                                        <img class="img-responsive " src="{{ App\getImage('services4.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pt-80 boxhowtext">
                                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                                        <h2>Social Media</h2>
                                                            <p>Somos apaixonados pelo que fazemos e nos preocupamos com o seu negócio e os resultados esperados por você. Somos uma pequena equipe com foco em grandes resultados, buscando sempre a melhoria contínua de nossos serviços impactando assim na melhoria de seu negócio.</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div>

                                </div>
                                <!-- Bottom Carousel Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="{{ App\getImage('desenvolvimento.png')}}" alt="">
                                    </li>
                                    <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="{{ App\getImage('services2.png')}}" alt="">
                                    </li>
                                    <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="{{ App\getImage('services3.png')}}" alt="">
                                    </li>
                                    <li data-target="#quote-carousel" data-slide-to="3"><img class="img-responsive" src="{{ App\getImage('services4.png')}}" alt="">
                                    </li>
                                </ol>

                                <!-- Carousel Buttons Next/Prev -->

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
