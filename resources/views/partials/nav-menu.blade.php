<nav id="nav-main-menu" class="nav-main-menu">
    <section class="container">

        <section class="brand col-xs-6 col-sm-3 col-md-2">
            <a href="{{ bloginfo('url') }}" class="logo">
                <img class="imglogo" src="{{ App\getImage('logoccm.png')}}" alt="Logo">
            </a>
        </section>

        {{-- Menu Responsive --}}
        <input class="burger-check" id="burger-check" type="checkbox">
        <label for="burger-check" class="burger"></label>

        <section class="nav-wrap col-xs-12 col-sm-12 col-md-9">
            {{ App\get_menu('primary_navigation') }}
        </section>
        <section class="social col-xs-12 col-sm-12 col-md-1">
            <a href="#">
                <i class="fa fa-facebook"></i>
            </a>
            <a href="#">
                <i class="fa fa-instagram"></i>
            </a>
        </section>
    </section>
</nav>
