<div id="contato" class="contato">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="titleccm">
                    <h1>ENTRE EM <span>CONTATO CONOSCO</span></h1>
                    <p class="subtitleccm">Precisando de ajuda? Entre em contato conosco.</p>
                </div>

                <div class="boxcontato pt-80">
                    <div class="container">
                    {{-- <div class="col-md-6 col-sm-12 col-xs-12"> --}}
                        {!! do_shortcode( '[contact-form-7 id="81" title="Contato"] ' )
                    !!}
                    </div>
                    {{-- </div> --}}
                </div>

            </div>
        </div>
    </div>

</div>
