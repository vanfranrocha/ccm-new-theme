@extends('layouts.app')

@section('content')
    <div id="main">
        @include('partials.sobre')

        @include('partials.comofazemos')

        @include('partials.cases')

        @include('partials.missaoevisao')

        @include('partials.clientes')

        @include('partials.info')

        @include('partials.time')

        @include('partials.contato')
    </div>
@endsection
