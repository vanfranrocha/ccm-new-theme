
/*!
 * Cube Portfolio - Responsive jQuery Grid Plugin
 *
 * version: 3.4.0 (15 November, 2015)
 * require: jQuery v1.7+
 *
 * Copyright 2013-2015, Mihai Buricea (http://scriptpie.com/cubeportfolio/live-preview/)
 * Licensed under CodeCanyon License (http://codecanyon.net/licenses)
 *
 */
!function(a,b,c,d){"use strict";function e(b,c,d){var f,g=this,h="cbp";if(a.data(b,"cubeportfolio"))throw new Error("cubeportfolio is already initialized. Destroy it before initialize again!");a.data(b,"cubeportfolio",g),g.options=a.extend({},a.fn.cubeportfolio.options,c),g.isAnimating=!0,g.defaultFilter=g.options.defaultFilter,g.registeredEvents=[],g.queue=[],g.addedWrapp=!1,a.isFunction(d)&&g.registerEvent("initFinish",d,!0),g.obj=b,g.$obj=a(b),f=g.$obj.children(),g.options.caption&&("expand"===g.options.caption||e["private"].modernBrowser||(g.options.caption="minimal"),h+=" cbp-caption-active cbp-caption-"+g.options.caption),g.$obj.addClass(h),(0===f.length||f.first().hasClass("cbp-item"))&&(g.wrapInner(g.obj,"cbp-wrapper"),g.addedWrapp=!0),g.$ul=g.$obj.children().addClass("cbp-wrapper"),g.wrapInner(g.obj,"cbp-wrapper-outer"),g.wrapper=g.$obj.children(".cbp-wrapper-outer"),g.blocks=g.$ul.children(".cbp-item"),g.blocksOn=g.blocks,g.wrapInner(g.blocks,"cbp-item-wrapper"),g.plugins=a.map(e.plugins,function(a){return a(g)}),g.loadImages(g.$obj,g.display)}a.extend(e.prototype,{storeData:function(b,c){var d=this;c=c||0,b.each(function(b,e){var f=a(e),g=f.width(),h=f.height();f.data("cbp",{index:c+b,wrapper:f.children(".cbp-item-wrapper"),widthInitial:g,heightInitial:h,width:g,height:h,widthAndGap:g+d.options.gapVertical,heightAndGap:h+d.options.gapHorizontal,left:null,leftNew:null,top:null,topNew:null,pack:!1})})},wrapInner:function(a,b){var e,f,g;if(b=b||"",!(a.length&&a.length<1))for(a.length===d&&(a=[a]),f=a.length-1;f>=0;f--){for(e=a[f],g=c.createElement("div"),g.setAttribute("class",b);e.childNodes.length;)g.appendChild(e.childNodes[0]);e.appendChild(g)}},removeAttrImage:function(a){a.removeAttribute("width"),a.removeAttribute("height"),a.removeAttribute("style")},loadImages:function(b,c){var d=this;requestAnimationFrame(function(){var e=b.find("img").map(function(b,c){return c.hasAttribute("width")&&c.hasAttribute("height")?(c.style.width=c.getAttribute("width")+"px",c.style.height=c.getAttribute("height")+"px",c.hasAttribute("data-cbp-src")?null:(null===d.checkSrc(c.src)?d.removeAttrImage(c):a("<img>").on("load.cbp error.cbp",function(){d.removeAttrImage(c)}).attr("src",c.src),null)):d.checkSrc(c.src)}),f=e.length;return 0===f?void c.call(d):void a.each(e,function(b,e){a("<img>").on("load.cbp error.cbp",function(){f--,0===f&&c.call(d)}).attr("src",e)})})},checkSrc:function(a){if(""===a)return null;var b=new Image;return b.src=a,b.complete&&b.naturalWidth!==d&&0!==b.naturalWidth?null:a},display:function(){var a=this;a.width=a.$obj.outerWidth(),a.storeData(a.blocks),a.triggerEvent("initStartRead"),a.triggerEvent("initStartWrite"),a.layoutAndAdjustment(),a.triggerEvent("initEndRead"),a.triggerEvent("initEndWrite"),a.$obj.addClass("cbp-ready"),a.runQueue("delayFrame",a.delayFrame)},delayFrame:function(){var a=this;requestAnimationFrame(function(){a.resizeEvent(),a.triggerEvent("initFinish"),a.isAnimating=!1,a.$obj.trigger("initComplete.cbp")})},resizeEvent:function(){var a,b=this;e["private"].initResizeEvent({instance:b,fn:function(){var b=this;b.triggerEvent("beforeResizeGrid"),a=b.$obj.outerWidth(),b.width!==a&&("alignCenter"===b.options.gridAdjustment&&(b.wrapper[0].style.maxWidth=""),b.width=a,b.layoutAndAdjustment(),b.triggerEvent("resizeGrid")),b.triggerEvent("resizeWindow")}})},gridAdjust:function(){var b=this;"responsive"===b.options.gridAdjustment?b.responsiveLayout():(b.blocks.removeAttr("style"),b.blocks.each(function(c,d){var e=a(d).data("cbp"),f=d.getBoundingClientRect(),g=b.columnWidthTruncate(f.right-f.left),h=Math.round(f.bottom-f.top);e.height=h,e.heightAndGap=h+b.options.gapHorizontal,e.width=g,e.widthAndGap=g+b.options.gapVertical}),b.widthAvailable=b.width+b.options.gapVertical),b.triggerEvent("gridAdjust")},layoutAndAdjustment:function(){var a=this;a.gridAdjust(),a.layout()},layout:function(){var a=this;a.computeBlocks(a.filterConcat(a.defaultFilter)),"slider"===a.options.layoutMode?(a.sliderLayoutReset(),a.sliderLayout()):(a.mosaicLayoutReset(),a.mosaicLayout()),a.positionateItems(),a.resizeMainContainer()},computeFilter:function(a){var b=this;b.computeBlocks(a),b.mosaicLayoutReset(),b.mosaicLayout(),b.filterLayout()},filterLayout:function(){var b=this;b.blocksOff.addClass("cbp-item-off"),b.blocksOn.removeClass("cbp-item-off").each(function(b,c){var d=a(c).data("cbp");d.left=d.leftNew,d.top=d.topNew,c.style.left=d.left+"px",c.style.top=d.top+"px"}),b.resizeMainContainer(),b.filterFinish()},filterFinish:function(){var a=this;a.blocksAreSorted&&a.sortBlocks(a.blocks,"index"),a.isAnimating=!1,a.$obj.trigger("filterComplete.cbp"),a.triggerEvent("filterFinish")},computeBlocks:function(a){var b=this;b.blocksOnInitial=b.blocksOn,b.blocksOn=b.blocks.filter(a),b.blocksOff=b.blocks.not(a),b.triggerEvent("computeBlocksFinish",a)},responsiveLayout:function(){var b=this;b.cols=b[a.isArray(b.options.mediaQueries)?"getColumnsBreakpoints":"getColumnsAuto"](),b.columnWidth=b.columnWidthTruncate((b.width+b.options.gapVertical)/b.cols),b.widthAvailable=b.columnWidth*b.cols,"mosaic"===b.options.layoutMode&&b.getMosaicWidthReference(),b.blocks.each(function(c,d){var e,f=a(d).data("cbp"),g=1;"mosaic"===b.options.layoutMode&&(g=b.getColsMosaic(f.widthInitial)),e=b.columnWidth*g-b.options.gapVertical,d.style.width=e+"px",f.width=e,f.widthAndGap=e+b.options.gapVertical,d.style.height=""});var c=[];b.blocks.each(function(b,d){var e=a(d),f=e.data("cbp").width;a.each(e.find("img").filter("[width][height]"),function(a,b){var d=f/parseInt(b.getAttribute("width"),10);c.push({el:b,width:f,height:Math.floor(parseInt(b.getAttribute("height"),10)*d)})})}),a.each(c,function(a,b){b.el.width=b.width,b.el.height=b.height,b.el.style.width=b.width+"px",b.el.style.height=b.height+"px"}),b.blocks.each(function(c,d){var e=a(d).data("cbp"),f=d.getBoundingClientRect(),g=Math.round(f.bottom-f.top);e.height=g,e.heightAndGap=g+b.options.gapHorizontal})},getMosaicWidthReference:function(){var b=this,c=[];b.blocks.each(function(b,d){var e=a(d).data("cbp");c.push(e.widthInitial)}),c.sort(function(a,b){return a-b}),c[0]?b.mosaicWidthReference=c[0]:b.mosaicWidthReference=b.columnWidth},getColsMosaic:function(a){var b=this;if(a===b.width)return b.cols;var c=a/b.mosaicWidthReference;return c=c%1>=.79?Math.ceil(c):Math.floor(c),Math.min(Math.max(c,1),b.cols)},getColumnsAuto:function(){var a=this;if(0===a.blocks.length)return 1;var b=a.blocks.first().data("cbp").widthInitial+a.options.gapVertical;return Math.max(Math.round(a.width/b),1)},getColumnsBreakpoints:function(){var b,c=this,e=c.width;return a.each(c.options.mediaQueries,function(a,c){return e>=c.width?(b=c.cols,!1):void 0}),b===d&&(b=c.options.mediaQueries[c.options.mediaQueries.length-1].cols),b},columnWidthTruncate:function(a){return Math.floor(a)},positionateItems:function(){var b,c=this;c.blocksOn.removeClass("cbp-item-off").each(function(c,d){b=a(d).data("cbp"),b.left=b.leftNew,b.top=b.topNew,d.style.left=b.left+"px",d.style.top=b.top+"px"}),c.blocksOff.addClass("cbp-item-off"),c.blocksAreSorted&&c.sortBlocks(c.blocks,"index")},resizeMainContainer:function(){var b,c=this,f=Math.max(c.freeSpaces.slice(-1)[0].topStart-c.options.gapHorizontal,0);return"alignCenter"===c.options.gridAdjustment&&(b=0,c.blocksOn.each(function(c,d){var e=a(d).data("cbp"),f=e.left+e.width;f>b&&(b=f)}),c.wrapper[0].style.maxWidth=b+"px"),f===c.height?void c.triggerEvent("resizeMainContainer"):(c.obj.style.height=f+"px",c.height!==d&&(e["private"].modernBrowser?c.$obj.one(e["private"].transitionend,function(){c.$obj.trigger("pluginResize.cbp")}):c.$obj.trigger("pluginResize.cbp")),c.height=f,void c.triggerEvent("resizeMainContainer"))},filterConcat:function(a){return a.replace(/\|/gi,"")},pushQueue:function(a,b){var c=this;c.queue[a]=c.queue[a]||[],c.queue[a].push(b)},runQueue:function(b,c){var d=this,e=d.queue[b]||[];a.when.apply(a,e).then(a.proxy(c,d))},clearQueue:function(a){var b=this;b.queue[a]=[]},registerEvent:function(a,b,c){var d=this;d.registeredEvents[a]||(d.registeredEvents[a]=[]),d.registeredEvents[a].push({func:b,oneTime:c||!1})},triggerEvent:function(a,b){var c,d,e=this;if(e.registeredEvents[a])for(c=0,d=e.registeredEvents[a].length;d>c;c++)e.registeredEvents[a][c].func.call(e,b),e.registeredEvents[a][c].oneTime&&(e.registeredEvents[a].splice(c,1),c--,d--)},addItems:function(b,c){var d=this;d.wrapInner(b,"cbp-item-wrapper"),b.addClass("cbp-item-loading").css({top:"100%",left:0}).appendTo(d.$ul),e["private"].modernBrowser?b.last().one(e["private"].animationend,function(){d.addItemsFinish(b,c)}):d.addItemsFinish(b,c),d.loadImages(b,function(){d.$obj.addClass("cbp-addItems"),d.storeData(b,d.blocks.length),a.merge(d.blocks,b),d.triggerEvent("addItemsToDOM",b),d.layoutAndAdjustment(),d.elems&&e["public"].showCounter.call(d.obj,d.elems)})},addItemsFinish:function(b,c){var d=this;d.isAnimating=!1,d.$obj.removeClass("cbp-addItems"),b.removeClass("cbp-item-loading"),a.isFunction(c)&&c.call(d,b)}}),a.fn.cubeportfolio=function(a,b,c){return this.each(function(){if("object"==typeof a||!a)return e["public"].init.call(this,a,b);if(e["public"][a])return e["public"][a].call(this,b,c);throw new Error("Method "+a+" does not exist on jquery.cubeportfolio.js")})},e.plugins={},a.fn.cubeportfolio.constructor=e}(jQuery,window,document),function(a,b,c,d){"use strict";var e=a.fn.cubeportfolio.constructor;a.extend(e.prototype,{mosaicLayoutReset:function(){var b=this;b.blocksAreSorted=!1,b.blocksOn.each(function(b,c){a(c).data("cbp").pack=!1})},mosaicLayout:function(){var a,b=this,c=b.blocksOn.length,d={};for(b.freeSpaces=[{leftStart:0,leftEnd:b.widthAvailable,topStart:0,topEnd:Math.pow(2,18)}],a=0;c>a;a++){if(d=b.getSpaceIndexAndBlock(),null===d)return b.sortBlocksToPreventGaps(),void b.mosaicLayout();b.generateF1F2(d.spaceIndex,d.dataBlock),b.generateG1G2G3G4(d.dataBlock),b.cleanFreeSpaces(),b.addHeightToBlocks()}b.blocksAreSorted&&b.sortBlocks(b.blocksOn,"topNew")},getSpaceIndexAndBlock:function(){var b=this,c=null;return a.each(b.freeSpaces,function(d,e){var f=e.leftEnd-e.leftStart,g=e.topEnd-e.topStart;return b.blocksOn.each(function(b,h){var i=a(h).data("cbp");if(i.pack!==!0)return i.widthAndGap<=f&&i.heightAndGap<=g?(i.pack=!0,c={spaceIndex:d,dataBlock:i},i.leftNew=e.leftStart,i.topNew=e.topStart,!1):void 0}),!b.blocksAreSorted&&b.options.sortToPreventGaps&&d>0?(c=null,!1):null!==c?!1:void 0}),c},generateF1F2:function(a,b){var c=this,d=c.freeSpaces[a],e={leftStart:d.leftStart+b.widthAndGap,leftEnd:d.leftEnd,topStart:d.topStart,topEnd:d.topEnd},f={leftStart:d.leftStart,leftEnd:d.leftEnd,topStart:d.topStart+b.heightAndGap,topEnd:d.topEnd};c.freeSpaces.splice(a,1),e.leftEnd>e.leftStart&&e.topEnd>e.topStart&&(c.freeSpaces.splice(a,0,e),a++),f.leftEnd>f.leftStart&&f.topEnd>f.topStart&&c.freeSpaces.splice(a,0,f)},generateG1G2G3G4:function(b){var c=this,d=[];a.each(c.freeSpaces,function(a,e){var f=c.intersectSpaces(e,b);return null===f?void d.push(e):(c.generateG1(e,f,d),c.generateG2(e,f,d),c.generateG3(e,f,d),void c.generateG4(e,f,d))}),c.freeSpaces=d},intersectSpaces:function(a,b){var c={leftStart:b.leftNew,leftEnd:b.leftNew+b.widthAndGap,topStart:b.topNew,topEnd:b.topNew+b.heightAndGap};if(a.leftStart===c.leftStart&&a.leftEnd===c.leftEnd&&a.topStart===c.topStart&&a.topEnd===c.topEnd)return null;var d=Math.max(a.leftStart,c.leftStart),e=Math.min(a.leftEnd,c.leftEnd),f=Math.max(a.topStart,c.topStart),g=Math.min(a.topEnd,c.topEnd);return d>=e||f>=g?null:{leftStart:d,leftEnd:e,topStart:f,topEnd:g}},generateG1:function(a,b,c){a.topStart!==b.topStart&&c.push({leftStart:a.leftStart,leftEnd:a.leftEnd,topStart:a.topStart,topEnd:b.topStart})},generateG2:function(a,b,c){a.leftEnd!==b.leftEnd&&c.push({leftStart:b.leftEnd,leftEnd:a.leftEnd,topStart:a.topStart,topEnd:a.topEnd})},generateG3:function(a,b,c){a.topEnd!==b.topEnd&&c.push({leftStart:a.leftStart,leftEnd:a.leftEnd,topStart:b.topEnd,topEnd:a.topEnd})},generateG4:function(a,b,c){a.leftStart!==b.leftStart&&c.push({leftStart:a.leftStart,leftEnd:b.leftStart,topStart:a.topStart,topEnd:a.topEnd})},cleanFreeSpaces:function(){var a=this;a.freeSpaces.sort(function(a,b){return a.topStart>b.topStart?1:a.topStart<b.topStart?-1:a.leftStart>b.leftStart?1:a.leftStart<b.leftStart?-1:0}),a.correctSubPixelValues(),a.removeNonMaximalFreeSpaces()},correctSubPixelValues:function(){var a,b,c,d,e=this;for(a=0,b=e.freeSpaces.length-1;b>a;a++)c=e.freeSpaces[a],d=e.freeSpaces[a+1],d.topStart-c.topStart<=1&&(d.topStart=c.topStart)},removeNonMaximalFreeSpaces:function(){var b=this;b.uniqueFreeSpaces(),b.freeSpaces=a.map(b.freeSpaces,function(c,d){return a.each(b.freeSpaces,function(a,b){return d!==a&&b.leftStart<=c.leftStart&&b.leftEnd>=c.leftEnd&&b.topStart<=c.topStart&&b.topEnd>=c.topEnd?(c=null,!1):void 0}),c})},uniqueFreeSpaces:function(){var b=this,c=[];a.each(b.freeSpaces,function(b,d){a.each(c,function(a,b){return b.leftStart===d.leftStart&&b.leftEnd===d.leftEnd&&b.topStart===d.topStart&&b.topEnd===d.topEnd?(d=null,!1):void 0}),null!==d&&c.push(d)}),b.freeSpaces=c},addHeightToBlocks:function(){var b=this;if(!(b.freeSpaces.length>1)){var c=b.freeSpaces[0].topStart;b.blocksOn.each(function(b,d){var e=a(d).data("cbp");if(e.pack===!0){var f=c-e.topNew-e.heightAndGap;0>f&&(d.style.height=e.height+f+"px")}})}},sortBlocksToPreventGaps:function(){var b=this;b.blocksAreSorted=!0,b.blocksOn.sort(function(b,c){var d=a(b).data("cbp"),e=a(c).data("cbp");return d.widthAndGap<e.widthAndGap?1:d.widthAndGap>e.widthAndGap?-1:d.heightAndGap<e.heightAndGap?1:d.heightAndGap>e.heightAndGap?-1:d.index>e.index?1:d.index<e.index?-1:void 0}),b.blocksOn.each(function(b,c){a(c).data("cbp").pack=!1,c.style.height=""})},sortBlocks:function(b,c){b.sort(function(b,d){var e=a(b).data("cbp"),f=a(d).data("cbp");return e[c]>f[c]?1:e[c]<f[c]?-1:e.leftNew>f.leftNew?1:e.leftNew<f.leftNew?-1:0})}})}(jQuery,window,document),jQuery.fn.cubeportfolio.options={filters:"",loadMore:"",loadMoreAction:"click",search:"",layoutMode:"grid",sortToPreventGaps:!1,drag:!0,auto:!1,autoTimeout:5e3,autoPauseOnHover:!0,showNavigation:!0,showPagination:!0,rewindNav:!0,scrollByPage:!1,defaultFilter:"*",filterDeeplinking:!1,animationType:"fadeOut",gridAdjustment:"responsive",mediaQueries:!1,gapHorizontal:10,gapVertical:10,caption:"pushTop",displayType:"fadeIn",displayTypeSpeed:400,lightboxDelegate:".cbp-lightbox",lightboxGallery:!0,lightboxTitleSrc:"data-title",lightboxCounter:'<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',singlePageDelegate:".cbp-singlePage",singlePageDeeplinking:!0,singlePageStickyNavigation:!0,singlePageCounter:'<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',singlePageAnimation:"left",singlePageCallback:function(a,b){},singlePageInlineDelegate:".cbp-singlePageInline",singlePageInlineDeeplinking:!1,singlePageInlinePosition:"top",singlePageInlineInFocus:!0,singlePageInlineCallback:function(a,b){},plugins:{}},function(a,b,c,d){"use strict";var e=a.fn.cubeportfolio.constructor;e["private"]={resizeEventArray:[],initResizeEvent:function(a){var b=e["private"];0===b.resizeEventArray.length&&b.resizeEvent(),b.resizeEventArray.push(a)},destroyResizeEvent:function(c){var d=e["private"],f=a.map(d.resizeEventArray,function(a,b){return a.instance!==c?a:void 0});d.resizeEventArray=f,0===d.resizeEventArray.length&&a(b).off("resize.cbp")},resizeEvent:function(){var c,d=e["private"];a(b).on("resize.cbp",function(){clearTimeout(c),c=setTimeout(function(){b.innerHeight!=screen.height&&a.each(d.resizeEventArray,function(a,b){b.fn.call(b.instance)})},50)})},checkInstance:function(b){var c=a.data(this,"cubeportfolio");if(!c)throw new Error("cubeportfolio is not initialized. Initialize it before calling "+b+" method!");return c.triggerEvent("publicMethod"),c},browserInfo:function(){var a,c,f,g=e["private"],h=navigator.appVersion;-1!==h.indexOf("MSIE 8.")?g.browser="ie8":-1!==h.indexOf("MSIE 9.")?g.browser="ie9":-1!==h.indexOf("MSIE 10.")?g.browser="ie10":b.ActiveXObject||"ActiveXObject"in b?g.browser="ie11":/android/gi.test(h)?g.browser="android":/iphone|ipad|ipod/gi.test(h)?g.browser="ios":/chrome/gi.test(h)?g.browser="chrome":g.browser="",f=g.styleSupport("perspective"),typeof f!==d&&(a=g.styleSupport("transition"),g.transitionend={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[a],c=g.styleSupport("animation"),g.animationend={WebkitAnimation:"webkitAnimationEnd",animation:"animationend"}[c],g.animationDuration={WebkitAnimation:"webkitAnimationDuration",animation:"animationDuration"}[c],g.animationDelay={WebkitAnimation:"webkitAnimationDelay",animation:"animationDelay"}[c],g.transform=g.styleSupport("transform"),a&&c&&g.transform&&(g.modernBrowser=!0))},styleSupport:function(a){var b,d="Webkit"+a.charAt(0).toUpperCase()+a.slice(1),e=c.createElement("div");return a in e.style?b=a:d in e.style&&(b=d),e=null,b}},e["private"].browserInfo()}(jQuery,window,document),function(a,b,c,d){"use strict";var e=a.fn.cubeportfolio.constructor;e["public"]={init:function(a,b){new e(this,a,b)},destroy:function(b){var c=e["private"].checkInstance.call(this,"destroy");c.triggerEvent("beforeDestroy"),a.removeData(this,"cubeportfolio"),c.blocks.removeData("cbp"),c.$obj.removeClass("cbp-ready").removeAttr("style"),c.$ul.removeClass("cbp-wrapper"),e["private"].destroyResizeEvent(c),c.$obj.off(".cbp"),c.blocks.removeClass("cbp-item-off").removeAttr("style"),c.blocks.find(".cbp-item-wrapper").children().unwrap(),c.options.caption&&c.$obj.removeClass("cbp-caption-active cbp-caption-"+c.options.caption),c.destroySlider(),c.$ul.unwrap(),c.addedWrapp&&c.blocks.unwrap(),a.each(c.plugins,function(a,b){"function"==typeof b.destroy&&b.destroy()}),a.isFunction(b)&&b.call(c),c.triggerEvent("afterDestroy")},filter:function(b,c){var f,g=e["private"].checkInstance.call(this,"filter");if(!g.isAnimating){if(g.isAnimating=!0,a.isFunction(c)&&g.registerEvent("filterFinish",c,!0),a.isFunction(b)){if(f=b.call(g,g.blocks),f===d)throw new Error("When you call cubeportfolio API `filter` method with a param of type function you must return the blocks that will be visible.")}else{if(g.options.filterDeeplinking){var h=location.href.replace(/#cbpf=(.*?)([#\?&]|$)/gi,"");location.href=h+"#cbpf="+encodeURIComponent(b),g.singlePage&&g.singlePage.url&&(g.singlePage.url=location.href)}g.defaultFilter=b,f=g.filterConcat(g.defaultFilter)}g.singlePageInline&&g.singlePageInline.isOpen?g.singlePageInline.close("promise",{callback:function(){g.computeFilter(f)}}):g.computeFilter(f)}},showCounter:function(b,c){var d=e["private"].checkInstance.call(this,"showCounter");d.elems=b,a.each(b,function(){var b,c=a(this),e=c.data("filter");b=d.blocks.filter(e).length,c.find(".cbp-filter-counter").text(b)}),a.isFunction(c)&&c.call(d)},appendItems:function(b,c){var d=e["private"].checkInstance.call(this,"appendItems"),f=a(b).filter(".cbp-item");return d.isAnimating||f.length<1?void(a.isFunction(c)&&c.call(d,f)):(d.isAnimating=!0,void(d.singlePageInline&&d.singlePageInline.isOpen?d.singlePageInline.close("promise",{callback:function(){d.addItems(f,c)}}):d.addItems(f,c)))}}}(jQuery,window,document),function(a,b,c,d){"use strict";var e=a.fn.cubeportfolio.constructor;a.extend(e.prototype,{updateSliderPagination:function(){var b,c,d=this;if(d.options.showPagination){for(b=Math.ceil(d.blocksOn.length/d.cols),d.navPagination.empty(),c=b-1;c>=0;c--)a("<div/>",{"class":"cbp-nav-pagination-item","data-slider-action":"jumpTo"}).appendTo(d.navPagination);d.navPaginationItems=d.navPagination.children()}d.enableDisableNavSlider()},destroySlider:function(){var b=this;"slider"===b.options.layoutMode&&(b.$obj.off(".cbp"),b.$obj.removeClass("cbp-mode-slider"),b.options.showNavigation&&(b.nav.off(".cbp"),b.nav.remove()),b.navPagination&&b.navPagination.remove(),b.$ul.removeAttr("style"),b.$ul.off(".cbp"),a(c).off(".cbp"),b.options.auto&&b.stopSliderAuto())},nextSlider:function(a){var b=this;if(b.isEndSlider()){if(!b.isRewindNav())return;b.sliderActive=0}else b.options.scrollByPage?b.sliderActive=Math.min(b.sliderActive+b.cols,b.blocksOn.length-b.cols):b.sliderActive+=1;b.goToSlider()},prevSlider:function(a){var b=this;if(b.isStartSlider()){if(!b.isRewindNav())return;b.sliderActive=b.blocksOn.length-b.cols}else b.options.scrollByPage?b.sliderActive=Math.max(0,b.sliderActive-b.cols):b.sliderActive-=1;b.goToSlider()},jumpToSlider:function(a){var b=this,c=Math.min(a.index()*b.cols,b.blocksOn.length-b.cols);c!==b.sliderActive&&(b.sliderActive=c,b.goToSlider())},jumpDragToSlider:function(a){var b,c,d,e=this,f=a>0?!0:!1;e.options.scrollByPage?(b=e.cols*e.columnWidth,c=e.cols):(b=e.columnWidth,c=1),a=Math.abs(a),d=Math.floor(a/b)*c,a%b>20&&(d+=c),f?e.sliderActive=Math.min(e.sliderActive+d,e.blocksOn.length-e.cols):e.sliderActive=Math.max(0,e.sliderActive-d),e.goToSlider()},isStartSlider:function(){return 0===this.sliderActive},isEndSlider:function(){var a=this;return a.sliderActive+a.cols>a.blocksOn.length-1},goToSlider:function(){var a=this;a.enableDisableNavSlider(),a.updateSliderPosition()},startSliderAuto:function(){var a=this;return a.isDrag?void a.stopSliderAuto():void(a.timeout=setTimeout(function(){a.nextSlider(),a.startSliderAuto()},a.options.autoTimeout))},stopSliderAuto:function(){clearTimeout(this.timeout)},enableDisableNavSlider:function(){var a,b,c=this;c.isRewindNav()||(b=c.isStartSlider()?"addClass":"removeClass",c.navPrev[b]("cbp-nav-stop"),b=c.isEndSlider()?"addClass":"removeClass",c.navNext[b]("cbp-nav-stop")),c.options.showPagination&&(a=c.options.scrollByPage?Math.ceil(c.sliderActive/c.cols):c.isEndSlider()?c.navPaginationItems.length-1:Math.floor(c.sliderActive/c.cols),c.navPaginationItems.removeClass("cbp-nav-pagination-active").eq(a).addClass("cbp-nav-pagination-active")),c.customPagination&&(a=c.options.scrollByPage?Math.ceil(c.sliderActive/c.cols):c.isEndSlider()?c.customPaginationItems.length-1:Math.floor(c.sliderActive/c.cols),c.customPaginationItems.removeClass(c.customPaginationClass).eq(a).addClass(c.customPaginationClass))},isRewindNav:function(){var a=this;return a.options.showNavigation?a.blocksOn.length<=a.cols?!1:a.options.rewindNav?!0:!1:!0},sliderItemsLength:function(){return this.blocksOn.length<=this.cols},sliderLayout:function(){var b=this;b.blocksOn.each(function(c,d){var e=a(d).data("cbp");e.leftNew=b.columnWidth*c,e.topNew=0,b.sliderFreeSpaces.push({topStart:e.heightAndGap})}),b.getFreeSpacesForSlider(),b.$ul.width(b.columnWidth*b.blocksOn.length-b.options.gapVertical)},getFreeSpacesForSlider:function(){var a=this;a.freeSpaces=a.sliderFreeSpaces.slice(a.sliderActive,a.sliderActive+a.cols),a.freeSpaces.sort(function(a,b){return a.topStart>b.topStart?1:a.topStart<b.topStart?-1:void 0})},updateSliderPosition:function(){var a=this,b=-a.sliderActive*a.columnWidth;e["private"].modernBrowser?a.$ul[0].style[e["private"].transform]="translate3d("+b+"px, 0px, 0)":a.$ul[0].style.left=b+"px",a.getFreeSpacesForSlider(),a.resizeMainContainer()},dragSlider:function(){function f(b){if(!q.sliderItemsLength()){if(u?p=b:b.preventDefault(),q.options.auto&&q.stopSliderAuto(),s)return void a(m).one("click.cbp",function(){return!1});m=a(b.target),k=j(b).x,l=0,n=-q.sliderActive*q.columnWidth,o=q.columnWidth*(q.blocksOn.length-q.cols),r.on(t.move,h),r.on(t.end,g),q.$obj.addClass("cbp-mode-slider-dragStart")}}function g(a){q.$obj.removeClass("cbp-mode-slider-dragStart"),requestAnimationFrame(function(){s=!0,0!==l?(m.one("click.cbp",function(){return!1}),q.jumpDragToSlider(l),q.$ul.one(e["private"].transitionend,i)):i.call(q),r.off(t.move),r.off(t.end)})}function h(a){l=k-j(a).x,(l>8||-8>l)&&a.preventDefault(),q.isDrag=!0;var b=n-l;0>l&&n>l?b=(n-l)/5:l>0&&-o>n-l&&(b=-o+(o+n-l)/5),e["private"].modernBrowser?q.$ul[0].style[e["private"].transform]="translate3d("+b+"px, 0px, 0)":q.$ul[0].style.left=b+"px"}function i(){if(s=!1,q.isDrag=!1,q.options.auto){if(q.mouseIsEntered)return;q.startSliderAuto()}}function j(a){return a.originalEvent!==d&&a.originalEvent.touches!==d&&(a=a.originalEvent.touches[0]),{x:a.pageX,y:a.pageY}}var k,l,m,n,o,p,q=this,r=a(c),s=!1,t={},u=!1;q.isDrag=!1,"ontouchstart"in b||navigator.maxTouchPoints>0||navigator.msMaxTouchPoints>0?(t={start:"touchstart.cbp",move:"touchmove.cbp",end:"touchend.cbp"},u=!0):t={start:"mousedown.cbp",move:"mousemove.cbp",end:"mouseup.cbp"},q.$ul.on(t.start,f)},sliderLayoutReset:function(){var a=this;a.freeSpaces=[],a.sliderFreeSpaces=[]}})}(jQuery,window,document),"function"!=typeof Object.create&&(Object.create=function(a){function b(){}return b.prototype=a,new b}),function(){for(var a=0,b=["moz","webkit"],c=0;c<b.length&&!window.requestAnimationFrame;c++)window.requestAnimationFrame=window[b[c]+"RequestAnimationFrame"],window.cancelAnimationFrame=window[b[c]+"CancelAnimationFrame"]||window[b[c]+"CancelRequestAnimationFrame"];window.requestAnimationFrame||(window.requestAnimationFrame=function(b,c){var d=(new Date).getTime(),e=Math.max(0,16-(d-a)),f=window.setTimeout(function(){b(d+e)},e);return a=d+e,f}),window.cancelAnimationFrame||(window.cancelAnimationFrame=function(a){clearTimeout(a)})}(),function(a,b,c,d){"use strict";function e(a){var b=this;b.parent=a,a.filterLayout=b.filterLayout,a.registerEvent("computeBlocksFinish",function(b){a.blocksOn2On=a.blocksOnInitial.filter(b),a.blocksOn2Off=a.blocksOnInitial.not(b)})}var f=a.fn.cubeportfolio.constructor;e.prototype.filterLayout=function(){function b(){c.blocks.removeClass("cbp-item-on2off cbp-item-off2on cbp-item-on2on").each(function(b,c){var d=a(c).data("cbp");d.left=d.leftNew,d.top=d.topNew,c.style.left=d.left+"px",c.style.top=d.top+"px",c.style[f["private"].transform]=""}),c.blocksOff.addClass("cbp-item-off"),c.$obj.removeClass("cbp-animation-"+c.options.animationType),c.filterFinish()}var c=this;c.$obj.addClass("cbp-animation-"+c.options.animationType),c.blocksOn2On.addClass("cbp-item-on2on").each(function(b,c){var d=a(c).data("cbp");c.style[f["private"].transform]="translate3d("+(d.leftNew-d.left)+"px, "+(d.topNew-d.top)+"px, 0)"}),c.blocksOn2Off.addClass("cbp-item-on2off"),c.blocksOff2On=c.blocksOn.filter(".cbp-item-off").removeClass("cbp-item-off").addClass("cbp-item-off2on").each(function(b,c){var d=a(c).data("cbp");c.style.left=d.leftNew+"px",c.style.top=d.topNew+"px"}),c.blocksOn2Off.length?c.blocksOn2Off.last().data("cbp").wrapper.one(f["private"].animationend,b):c.blocksOff2On.length?c.blocksOff2On.last().data("cbp").wrapper.one(f["private"].animationend,b):b(),c.resizeMainContainer()},e.prototype.destroy=function(){var a=this.parent;a.$obj.removeClass("cbp-animation-"+a.options.animationType)},f.plugins.animationClassic=function(b){return!f["private"].modernBrowser||a.inArray(b.options.animationType,["boxShadow","fadeOut","flipBottom","flipOut","quicksand","scaleSides","skew"])<0?null:new e(b)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(a){var b=this;b.parent=a,a.filterLayout=b.filterLayout}var f=a.fn.cubeportfolio.constructor;e.prototype.filterLayout=function(){function b(){c.wrapper[0].removeChild(d),"sequentially"===c.options.animationType&&c.blocksOn.each(function(b,c){a(c).data("cbp").wrapper[0].style[f["private"].animationDelay]=""}),c.$obj.removeClass("cbp-animation-"+c.options.animationType),c.filterFinish()}var c=this,d=c.$ul[0].cloneNode(!0);d.setAttribute("class","cbp-wrapper-helper"),c.wrapper[0].insertBefore(d,c.$ul[0]),requestAnimationFrame(function(){c.$obj.addClass("cbp-animation-"+c.options.animationType),c.blocksOff.addClass("cbp-item-off"),c.blocksOn.removeClass("cbp-item-off").each(function(b,d){var e=a(d).data("cbp");e.left=e.leftNew,e.top=e.topNew,d.style.left=e.left+"px",d.style.top=e.top+"px","sequentially"===c.options.animationType&&(e.wrapper[0].style[f["private"].animationDelay]=60*b+"ms")}),c.blocksOn.length?c.blocksOn.last().data("cbp").wrapper.one(f["private"].animationend,b):c.blocksOnInitial.length?c.blocksOnInitial.last().data("cbp").wrapper.one(f["private"].animationend,b):b(),c.resizeMainContainer()})},e.prototype.destroy=function(){var a=this.parent;a.$obj.removeClass("cbp-animation-"+a.options.animationType)},f.plugins.animationClone=function(b){return!f["private"].modernBrowser||a.inArray(b.options.animationType,["fadeOutTop","slideLeft","sequentially"])<0?null:new e(b)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(a){var b=this;b.parent=a,a.filterLayout=b.filterLayout}var f=a.fn.cubeportfolio.constructor;e.prototype.filterLayout=function(){function b(){c.wrapper[0].removeChild(d[0]),c.$obj.removeClass("cbp-animation-"+c.options.animationType),c.blocks.each(function(b,c){a(c).data("cbp").wrapper[0].style[f["private"].animationDelay]=""}),c.filterFinish()}var c=this,d=c.$ul.clone(!0,!0);d[0].setAttribute("class","cbp-wrapper-helper"),c.wrapper[0].insertBefore(d[0],c.$ul[0]);var e=d.find(".cbp-item").not(".cbp-item-off");c.sortBlocks(e,"top"),e.children(".cbp-item-wrapper").each(function(a,b){b.style[f["private"].animationDelay]=50*a+"ms"}),requestAnimationFrame(function(){c.$obj.addClass("cbp-animation-"+c.options.animationType),c.blocksOff.addClass("cbp-item-off"),c.blocksOn.removeClass("cbp-item-off").each(function(b,c){var d=a(c).data("cbp");d.left=d.leftNew,d.top=d.topNew,c.style.left=d.left+"px",c.style.top=d.top+"px",d.wrapper[0].style[f["private"].animationDelay]=50*b+"ms"});var d=c.blocksOn.length,g=e.length;0===d&&0===g?b():g>d?e.last().children(".cbp-item-wrapper").one(f["private"].animationend,b):c.blocksOn.last().data("cbp").wrapper.one(f["private"].animationend,b),c.resizeMainContainer()})},e.prototype.destroy=function(){var a=this.parent;a.$obj.removeClass("cbp-animation-"+a.options.animationType)},f.plugins.animationCloneDelay=function(b){return!f["private"].modernBrowser||a.inArray(b.options.animationType,["3dflip","flipOutDelay","foldLeft","frontRow","rotateRoom","rotateSides","scaleDown","slideDelay","unfold"])<0?null:new e(b)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(a){var b=this;b.parent=a,a.filterLayout=b.filterLayout}var f=a.fn.cubeportfolio.constructor;e.prototype.filterLayout=function(){function b(){c.wrapper[0].removeChild(d),c.$obj.removeClass("cbp-animation-"+c.options.animationType),c.filterFinish()}var c=this,d=c.$ul[0].cloneNode(!0);d.setAttribute("class","cbp-wrapper-helper"),c.wrapper[0].insertBefore(d,c.$ul[0]),requestAnimationFrame(function(){c.$obj.addClass("cbp-animation-"+c.options.animationType),c.blocksOff.addClass("cbp-item-off"),c.blocksOn.removeClass("cbp-item-off").each(function(b,c){var d=a(c).data("cbp");d.left=d.leftNew,d.top=d.topNew,c.style.left=d.left+"px",c.style.top=d.top+"px"}),c.blocksOn.length?c.$ul.one(f["private"].animationend,b):c.blocksOnInitial.length?a(d).one(f["private"].animationend,b):b(),c.resizeMainContainer()})},e.prototype.destroy=function(){var a=this.parent;a.$obj.removeClass("cbp-animation-"+a.options.animationType)},f.plugins.animationWrapper=function(b){return!f["private"].modernBrowser||a.inArray(b.options.animationType,["bounceBottom","bounceLeft","bounceTop","moveLeft"])<0?null:new e(b)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,b.registerEvent("initFinish",function(){b.$obj.on("click.cbp",".cbp-caption-defaultWrap",function(c){if(c.preventDefault(),!b.isAnimating){b.isAnimating=!0;var d=a(this),e=d.next(),f=d.parent(),g={position:"relative",height:e.outerHeight(!0)},h={position:"relative",height:0};if(b.$obj.addClass("cbp-caption-expand-active"),f.hasClass("cbp-caption-expand-open")){var i=h;h=g,g=i,f.removeClass("cbp-caption-expand-open")}e.css(g),b.$obj.one("pluginResize.cbp",function(){b.isAnimating=!1,b.$obj.removeClass("cbp-caption-expand-active"),0===g.height&&(f.removeClass("cbp-caption-expand-open"),e.attr("style",""))}),b.layoutAndAdjustment(),e.css(h),requestAnimationFrame(function(){f.addClass("cbp-caption-expand-open"),e.css(g),b.triggerEvent("gridAdjust"),b.triggerEvent("resizeGrid")})}})},!0)}var f=a.fn.cubeportfolio.constructor;e.prototype.destroy=function(){this.parent.$obj.find(".cbp-caption-defaultWrap").off("click.cbp").parent().removeClass("cbp-caption-expand-active");
},f.plugins.captionExpand=function(a){return"expand"!==a.options.caption?null:new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=a.Deferred();b.pushQueue("delayFrame",c),b.registerEvent("initEndWrite",function(){b.blocksOn.each(function(a,c){c.style[f["private"].animationDelay]=a*b.options.displayTypeSpeed+"ms"}),b.$obj.addClass("cbp-displayType-bottomToTop"),b.blocksOn.last().one(f["private"].animationend,function(){b.$obj.removeClass("cbp-displayType-bottomToTop"),b.blocksOn.each(function(a,b){b.style[f["private"].animationDelay]=""}),c.resolve()})},!0)}var f=a.fn.cubeportfolio.constructor;f.plugins.displayBottomToTop=function(a){return f["private"].modernBrowser&&"bottomToTop"===a.options.displayType&&0!==a.blocksOn.length?new e(a):null}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=a.Deferred();b.pushQueue("delayFrame",c),b.registerEvent("initEndWrite",function(){b.obj.style[f["private"].animationDuration]=b.options.displayTypeSpeed+"ms",b.$obj.addClass("cbp-displayType-fadeIn"),b.$obj.one(f["private"].animationend,function(){b.$obj.removeClass("cbp-displayType-fadeIn"),b.obj.style[f["private"].animationDuration]="",c.resolve()})},!0)}var f=a.fn.cubeportfolio.constructor;f.plugins.displayFadeIn=function(a){return!f["private"].modernBrowser||"lazyLoading"!==a.options.displayType&&"fadeIn"!==a.options.displayType||0===a.blocksOn.length?null:new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=a.Deferred();b.pushQueue("delayFrame",c),b.registerEvent("initEndWrite",function(){b.obj.style[f["private"].animationDuration]=b.options.displayTypeSpeed+"ms",b.$obj.addClass("cbp-displayType-fadeInToTop"),b.$obj.one(f["private"].animationend,function(){b.$obj.removeClass("cbp-displayType-fadeInToTop"),b.obj.style[f["private"].animationDuration]="",c.resolve()})},!0)}var f=a.fn.cubeportfolio.constructor;f.plugins.displayFadeInToTop=function(a){return f["private"].modernBrowser&&"fadeInToTop"===a.options.displayType&&0!==a.blocksOn.length?new e(a):null}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=a.Deferred();b.pushQueue("delayFrame",c),b.registerEvent("initEndWrite",function(){b.blocksOn.each(function(a,c){c.style[f["private"].animationDelay]=a*b.options.displayTypeSpeed+"ms"}),b.$obj.addClass("cbp-displayType-sequentially"),b.blocksOn.last().one(f["private"].animationend,function(){b.$obj.removeClass("cbp-displayType-sequentially"),b.blocksOn.each(function(a,b){b.style[f["private"].animationDelay]=""}),c.resolve()})},!0)}var f=a.fn.cubeportfolio.constructor;f.plugins.displaySequentially=function(a){return f["private"].modernBrowser&&"sequentially"===a.options.displayType&&0!==a.blocksOn.length?new e(a):null}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,c.filters=a(b.options.filters),c.filterData=[],c.filterFromUrl(),c.registerFilter()}var f=a.fn.cubeportfolio.constructor;e.prototype.registerFilter=function(){var b=this,c=b.parent,d=c.defaultFilter.split("|");b.wrap=b.filters.find(".cbp-l-filters-dropdownWrap").on({"mouseover.cbp":function(){a(this).addClass("cbp-l-filters-dropdownWrap-open")},"mouseleave.cbp":function(){a(this).removeClass("cbp-l-filters-dropdownWrap-open")}}),b.filters.each(function(e,f){var g=a(f),h="*",i=g.find(".cbp-filter-item"),j={};g.hasClass("cbp-l-filters-dropdown")&&(j.wrap=g.find(".cbp-l-filters-dropdownWrap"),j.header=g.find(".cbp-l-filters-dropdownHeader"),j.headerText=j.header.text()),c.$obj.cubeportfolio("showCounter",i),a.each(d,function(a,b){return i.filter('[data-filter="'+b+'"]').length?(h=b,d.splice(a,1),!1):void 0}),a.data(f,"filterName",h),b.filterData.push(f),b.filtersCallback(j,i.filter('[data-filter="'+h+'"]')),i.on("click.cbp",function(){var d=a(this);if(!d.hasClass("cbp-filter-item-active")&&!c.isAnimating){b.filtersCallback(j,d),a.data(f,"filterName",d.data("filter"));var e=a.map(b.filterData,function(b,c){var d=a.data(b,"filterName");return""!==d&&"*"!==d?d:null});e.length<1&&(e=["*"]);var g=e.join("|");c.defaultFilter!==g&&c.$obj.cubeportfolio("filter",g)}})})},e.prototype.filtersCallback=function(b,c){a.isEmptyObject(b)||(b.wrap.trigger("mouseleave.cbp"),b.headerText?b.headerText="":b.header.html(c.html())),c.addClass("cbp-filter-item-active").siblings().removeClass("cbp-filter-item-active")},e.prototype.filterFromUrl=function(){var a=/#cbpf=(.*?)([#\?&]|$)/gi.exec(location.href);null!==a&&(this.parent.defaultFilter=decodeURIComponent(a[1]))},e.prototype.destroy=function(){var a=this;a.filters.find(".cbp-filter-item").off(".cbp"),a.wrap.off(".cbp")},f.plugins.filters=function(a){return""===a.options.filters?null:new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,c.options=a.extend({},g,c.parent.options.plugins.inlineSlider),c.runInit(),b.registerEvent("addItemsToDOM",function(){c.runInit()})}function f(a){var b=this;a.hasClass("cbp-slider-inline-ready")||(a.addClass("cbp-slider-inline-ready"),b.items=a.find(".cbp-slider-wrapper").children(".cbp-slider-item"),b.active=b.items.filter(".cbp-slider-item--active").index(),b.total=b.items.length-1,b.updateLeft(),a.find(".cbp-slider-next").on("click.cbp",function(a){a.preventDefault(),b.active<b.total?(b.active++,b.updateLeft()):b.active===b.total&&(b.active=0,b.updateLeft())}),a.find(".cbp-slider-prev").on("click.cbp",function(a){a.preventDefault(),b.active>0?(b.active--,b.updateLeft()):0===b.active&&(b.active=b.total,b.updateLeft())}))}var g={},h=a.fn.cubeportfolio.constructor;f.prototype.updateLeft=function(){var a=this;a.items.removeClass("cbp-slider-item--active"),a.items.eq(a.active).addClass("cbp-slider-item--active"),a.items.each(function(b,c){c.style.left=b-a.active+"00%"})},e.prototype.runInit=function(){var b=this;b.parent.$obj.find(".cbp-slider-inline").not(".cbp-slider-inline-ready").each(function(c,d){var e=a(d),g=e.find(".cbp-slider-item--active").find("img")[0];g.hasAttribute("data-cbp-src")?b.parent.$obj.on("lazyLoad.cbp",function(a,b){b.src===g.src&&new f(e)}):new f(e)})},e.prototype.destroy=function(){var b=this;b.parent.$obj.find(".cbp-slider-next").off("click.cbp"),b.parent.$obj.find(".cbp-slider-prev").off("click.cbp"),b.parent.$obj.off("lazyLoad.cbp"),b.parent.$obj.find(".cbp-slider-inline").each(function(b,c){var d=a(c);d.removeClass("cbp-slider-inline-ready");var e=d.find(".cbp-slider-item");e.removeClass("cbp-slider-item--active"),e.removeAttr("style"),e.eq(0).addClass("cbp-slider-item--active")})},h.plugins.inlineSlider=function(a){return new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(c){var d=this;d.window=a(b),d.parent=c,d.options=a.extend({},f,d.parent.options.plugins.lazyLoad),c.registerEvent("initEndWrite",function(){var a;d.triggerImg(),c.registerEvent("resizeMainContainer",function(){d.triggerImg()}),d.window.on("scroll.cbp",function(){clearTimeout(a),a=setTimeout(function(){d.triggerImg()},300)})},!0)}var f={loadingClass:"cbp-lazyload",threshold:0},g=a.fn.cubeportfolio.constructor;e.prototype.triggerImg=function(){var b=this,c=b.parent.$obj.find("img").filter("[data-cbp-src]");0!==c.length&&(b.screenHeight=b.window.height(),c.each(function(c,d){var e=a(d.parentNode);if(!b.isElementInScreen(d))return void e.addClass(b.options.loadingClass);var f=d.getAttribute("data-cbp-src");null===b.parent.checkSrc(f)?(b.removeLazy(d,f),e.removeClass(b.options.loadingClass)):(e.addClass(b.options.loadingClass),a("<img>").on("load.cbp error.cbp",function(){b.removeLazy(d,f,e)}).attr("src",f))}))},e.prototype.removeLazy=function(b,c,d){var e=this;b.src=c,b.removeAttribute("data-cbp-src"),e.parent.removeAttrImage(b),e.parent.$obj.trigger("lazyLoad.cbp",b),d&&(g["private"].modernBrowser?a(b).one(g["private"].transitionend,function(){d.removeClass(e.options.loadingClass)}):d.removeClass(e.options.loadingClass))},e.prototype.isElementInScreen=function(a){var b=this,c=a.getBoundingClientRect(),d=c.bottom+b.options.threshold,e=b.screenHeight+d-(c.top-b.options.threshold);return d>=0&&e>=d},e.prototype.destroy=function(){var a=this;a.window.off("scroll.cbp")},g.plugins.lazyLoad=function(a){return new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,c.loadMore=a(b.options.loadMore).find(".cbp-l-loadMore-link"),c.loadMore.length&&c[b.options.loadMoreAction]()}var f=a.fn.cubeportfolio.constructor;e.prototype.click=function(){var b=this,c=0;b.loadMore.on("click.cbp",function(d){var e=a(this);d.preventDefault(),b.parent.isAnimating||e.hasClass("cbp-l-loadMore-stop")||(e.addClass("cbp-l-loadMore-loading"),c++,a.ajax({url:b.loadMore.attr("href")+"?block="+c,type:"GET",dataType:"HTML"}).done(function(a){var d=a.replace(/(\r\n|\n|\r)/gm,""),f=d.indexOf("cbp-loadMore-block"+c);if(-1===f)return void e.addClass("cbp-l-loadMore-stop");var g,h=d.indexOf(">",f)+1,i=d.indexOf("cbp-loadMore-block"+(c+1));g=-1===i?d.lastIndexOf("</"):d.lastIndexOf("</",i),b.parent.$obj.cubeportfolio("appendItems",d.substring(h,g),function(){e.removeClass("cbp-l-loadMore-loading"),-1===i&&e.addClass("cbp-l-loadMore-stop")})}).fail(function(){}))})},e.prototype.auto=function(){var c=this;c.parent.$obj.on("initComplete.cbp",function(){Object.create({init:function(){var d=this;d.isActive=!1,d.numberOfClicks=0,c.loadMore.addClass("cbp-l-loadMore-loading"),d.window=a(b),d.addEvents(),d.getNewItems()},addEvents:function(){var a,b=this;c.loadMore.on("click.cbp",function(a){a.preventDefault()}),b.window.on("scroll.loadMoreObject",function(){clearTimeout(a),a=setTimeout(function(){c.parent.isAnimating||b.getNewItems()},80)}),c.parent.$obj.on("filterComplete.cbp",function(){b.getNewItems()})},getNewItems:function(){var b,d,e=this;e.isActive||c.loadMore.hasClass("cbp-l-loadMore-stop")||(b=c.loadMore.offset().top-200,d=e.window.scrollTop()+e.window.height(),b>d||(e.isActive=!0,e.numberOfClicks++,a.ajax({url:c.loadMore.attr("href")+"?block="+e.numberOfClicks,type:"GET",dataType:"HTML",cache:!0}).done(function(a){var b=a.replace(/(\r\n|\n|\r)/gm,""),d=b.indexOf("cbp-loadMore-block"+e.numberOfClicks);if(-1===d)return void c.loadMore.addClass("cbp-l-loadMore-stop");var f,g=b.indexOf(">",d)+1,h=b.indexOf("cbp-loadMore-block"+(e.numberOfClicks+1));f=-1===h?b.lastIndexOf("</"):b.lastIndexOf("</",h),c.parent.$obj.cubeportfolio("appendItems",b.substring(g,f),function(){-1===h?(c.loadMore.addClass("cbp-l-loadMore-stop"),e.window.off("scroll.loadMoreObject"),c.parent.$obj.off("filterComplete.cbp")):(e.isActive=!1,e.window.trigger("scroll.loadMoreObject"))})}).fail(function(){e.isActive=!1})))}}).init()})},e.prototype.destroy=function(){var c=this;c.loadMore.off(".cbp"),a(b).off("scroll.loadMoreObject")},f.plugins.loadMore=function(a){return""===a.options.loadMore?null:new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(a){var b=this;b.parent=a,a.options.lightboxShowCounter===!1&&(a.options.lightboxCounter=""),a.options.singlePageShowCounter===!1&&(a.options.singlePageCounter=""),a.registerEvent("initStartRead",function(){b.run()},!0)}var f=a.fn.cubeportfolio.constructor,g={init:function(b,d){var e,f=this;if(f.cubeportfolio=b,f.type=d,f.isOpen=!1,f.options=f.cubeportfolio.options,"lightbox"===d&&f.cubeportfolio.registerEvent("resizeWindow",function(){f.resizeImage()}),"singlePageInline"!==d){if(f.createMarkup(),"singlePage"===d&&(f.cubeportfolio.registerEvent("resizeWindow",function(){if(f.options.singlePageStickyNavigation){var a=f.wrap[0].clientWidth;a>0&&(f.navigationWrap.width(a),f.navigation.width(a))}}),f.options.singlePageDeeplinking)){f.url=location.href,"#"===f.url.slice(-1)&&(f.url=f.url.slice(0,-1));var g=f.url.split("#cbp="),h=g.shift();if(a.each(g,function(b,c){return f.cubeportfolio.blocksOn.each(function(b,d){var g=a(d).find(f.options.singlePageDelegate+'[href="'+c+'"]');return g.length?(e=g,!1):void 0}),e?!1:void 0}),e){f.url=h;var i=e,j=i.attr("data-cbp-singlePage"),k=[];j?k=i.closest(a(".cbp-item")).find('[data-cbp-singlePage="'+j+'"]'):f.cubeportfolio.blocksOn.each(function(b,c){var d=a(c);d.not(".cbp-item-off")&&d.find(f.options.singlePageDelegate).each(function(b,c){a(c).attr("data-cbp-singlePage")||k.push(c)})}),f.openSinglePage(k,e[0])}else if(g.length){var l=c.createElement("a");l.setAttribute("href",g[0]),f.openSinglePage([l],l)}}}else if(f.height=0,f.createMarkupSinglePageInline(),f.cubeportfolio.registerEvent("resizeGrid",function(){f.isOpen&&f.close()}),f.options.singlePageInlineDeeplinking){f.url=location.href,"#"===f.url.slice(-1)&&(f.url=f.url.slice(0,-1));var g=f.url.split("#cbpi="),h=g.shift();a.each(g,function(b,c){return f.cubeportfolio.blocksOn.each(function(b,d){var g=a(d).find(f.options.singlePageInlineDelegate+'[href="'+c+'"]');return g.length?(e=g,!1):void 0}),e?!1:void 0}),e&&f.cubeportfolio.registerEvent("initFinish",function(){f.openSinglePageInline(f.cubeportfolio.blocksOn,e[0])},!0)}},createMarkup:function(){var b=this,d="";"singlePage"===b.type&&"left"!==b.options.singlePageAnimation&&(d=" cbp-popup-singlePage-"+b.options.singlePageAnimation),b.wrap=a("<div/>",{"class":"cbp-popup-wrap cbp-popup-"+b.type+d,"data-action":"lightbox"===b.type?"close":""}).on("click.cbp",function(c){if(!b.stopEvents){var d=a(c.target).attr("data-action");b[d]&&(b[d](),c.preventDefault())}}),b.content=a("<div/>",{"class":"cbp-popup-content"}).appendTo(b.wrap),a("<div/>",{"class":"cbp-popup-loadingBox"}).appendTo(b.wrap),"ie8"===f["private"].browser&&(b.bg=a("<div/>",{"class":"cbp-popup-ie8bg","data-action":"lightbox"===b.type?"close":""}).appendTo(b.wrap)),b.navigationWrap=a("<div/>",{"class":"cbp-popup-navigation-wrap"}).appendTo(b.wrap),b.navigation=a("<div/>",{"class":"cbp-popup-navigation"}).appendTo(b.navigationWrap),b.closeButton=a("<div/>",{"class":"cbp-popup-close",title:"Close (Esc arrow key)","data-action":"close"}).appendTo(b.navigation),b.nextButton=a("<div/>",{"class":"cbp-popup-next",title:"Next (Right arrow key)","data-action":"next"}).appendTo(b.navigation),b.prevButton=a("<div/>",{"class":"cbp-popup-prev",title:"Previous (Left arrow key)","data-action":"prev"}).appendTo(b.navigation),"singlePage"===b.type&&(b.options.singlePageCounter&&(b.counter=a(b.options.singlePageCounter).appendTo(b.navigation),b.counter.text("")),b.content.on("click.cbp",b.options.singlePageDelegate,function(a){a.preventDefault();var c,d=b.dataArray.length,e=this.getAttribute("href");for(c=0;d>c&&b.dataArray[c].url!==e;c++);b.singlePageJumpTo(c-b.current)}),b.wrap.on("mousewheel.cbp DOMMouseScroll.cbp",function(a){a.stopImmediatePropagation()})),a(c).on("keydown.cbp",function(a){b.isOpen&&(b.stopEvents||(37===a.keyCode?b.prev():39===a.keyCode?b.next():27===a.keyCode&&b.close()))})},createMarkupSinglePageInline:function(){var b=this;b.wrap=a("<div/>",{"class":"cbp-popup-singlePageInline"}).on("click.cbp",function(c){if(!b.stopEvents){var d=a(c.target).attr("data-action");d&&b[d]&&(b[d](),c.preventDefault())}}),b.content=a("<div/>",{"class":"cbp-popup-content"}).appendTo(b.wrap),b.navigation=a("<div/>",{"class":"cbp-popup-navigation"}).appendTo(b.wrap),b.closeButton=a("<div/>",{"class":"cbp-popup-close",title:"Close (Esc arrow key)","data-action":"close"}).appendTo(b.navigation)},destroy:function(){var b=this,d=a("body");a(c).off("keydown.cbp"),d.off("click.cbp",b.options.lightboxDelegate),d.off("click.cbp",b.options.singlePageDelegate),b.content.off("click.cbp",b.options.singlePageDelegate),b.cubeportfolio.$obj.off("click.cbp",b.options.singlePageInlineDelegate),b.cubeportfolio.$obj.off("click.cbp",b.options.lightboxDelegate),b.cubeportfolio.$obj.off("click.cbp",b.options.singlePageDelegate),b.cubeportfolio.$obj.removeClass("cbp-popup-isOpening"),b.cubeportfolio.$obj.find(".cbp-item").removeClass("cbp-singlePageInline-active"),b.wrap.remove()},openLightbox:function(d,e){var f,g,h=this,i=0,j=[];if(!h.isOpen){if(h.isOpen=!0,h.stopEvents=!1,h.dataArray=[],h.current=null,f=e.getAttribute("href"),null===f)throw new Error("HEI! Your clicked element doesn't have a href attribute.");a.each(d,function(b,c){var d,e=c.getAttribute("href"),g=e,k="isImage";if(-1===a.inArray(e,j)){if(f===e)h.current=i;else if(!h.options.lightboxGallery)return;/youtube/i.test(e)?(d=e.substring(e.lastIndexOf("v=")+2),/autoplay=/i.test(d)||(d+="&autoplay=1"),d=d.replace(/\?|&/,"?"),g="//www.youtube.com/embed/"+d,k="isYoutube"):/vimeo\.com/i.test(e)?(d=e.substring(e.lastIndexOf("/")+1),/autoplay=/i.test(d)||(d+="&autoplay=1"),d=d.replace(/\?|&/,"?"),g="//player.vimeo.com/video/"+d,k="isVimeo"):/www\.ted\.com/i.test(e)?(g="http://embed.ted.com/talks/"+e.substring(e.lastIndexOf("/")+1)+".html",k="isTed"):/soundcloud\.com/i.test(e)?(g=e,k="isSoundCloud"):/(\.mp4)|(\.ogg)|(\.ogv)|(\.webm)/i.test(e)?(g=-1!==e.indexOf("|")?e.split("|"):e.split("%7C"),k="isSelfHostedVideo"):/\.mp3$/i.test(e)&&(g=e,k="isSelfHostedAudio"),h.dataArray.push({src:g,title:c.getAttribute(h.options.lightboxTitleSrc),type:k}),i++}j.push(e)}),h.counterTotal=h.dataArray.length,1===h.counterTotal?(h.nextButton.hide(),h.prevButton.hide(),h.dataActionImg=""):(h.nextButton.show(),h.prevButton.show(),h.dataActionImg='data-action="next"'),h.wrap.appendTo(c.body),h.scrollTop=a(b).scrollTop(),h.originalStyle=a("html").attr("style"),a("html").css({overflow:"hidden",paddingRight:b.innerWidth-a(c).width()}),h.wrap.show(),g=h.dataArray[h.current],h[g.type](g)}},openSinglePage:function(d,e){var g,h=this,i=0,j=[];if(!h.isOpen){if(h.cubeportfolio.singlePageInline&&h.cubeportfolio.singlePageInline.isOpen&&h.cubeportfolio.singlePageInline.close(),h.isOpen=!0,h.stopEvents=!1,h.dataArray=[],h.current=null,g=e.getAttribute("href"),null===g)throw new Error("HEI! Your clicked element doesn't have a href attribute.");if(a.each(d,function(b,c){var d=c.getAttribute("href");-1===a.inArray(d,j)&&(g===d&&(h.current=i),h.dataArray.push({url:d,element:c}),i++),j.push(d)}),h.counterTotal=h.dataArray.length,1===h.counterTotal?(h.nextButton.hide(),h.prevButton.hide()):(h.nextButton.show(),h.prevButton.show()),h.wrap.appendTo(c.body),h.scrollTop=a(b).scrollTop(),a("html").css({overflow:"hidden",paddingRight:b.innerWidth-a(c).width()}),h.wrap.scrollTop(0),h.wrap.show(),h.finishOpen=2,h.navigationMobile=a(),h.wrap.one(f["private"].transitionend,function(){var b;h.options.singlePageStickyNavigation&&(h.wrap.addClass("cbp-popup-singlePage-sticky"),b=h.wrap[0].clientWidth,h.navigationWrap.width(b),("android"===f["private"].browser||"ios"===f["private"].browser)&&(h.navigationMobile=a("<div/>",{"class":"cbp-popup-singlePage cbp-popup-singlePage-sticky",id:h.wrap.attr("id")}).on("click.cbp",function(b){if(!h.stopEvents){var c=a(b.target).attr("data-action");h[c]&&(h[c](),b.preventDefault())}}),h.navigationMobile.appendTo(c.body).append(h.navigationWrap))),h.finishOpen--,h.finishOpen<=0&&h.updateSinglePageIsOpen.call(h)}),"ie8"===f["private"].browser||"ie9"===f["private"].browser){if(h.options.singlePageStickyNavigation){var k=h.wrap[0].clientWidth;h.navigationWrap.width(k),setTimeout(function(){h.wrap.addClass("cbp-popup-singlePage-sticky")},1e3)}h.finishOpen--}h.wrap.addClass("cbp-popup-loading"),h.wrap.offset(),h.wrap.addClass("cbp-popup-singlePage-open"),h.options.singlePageDeeplinking&&(h.url=h.url.split("#cbp=")[0],location.href=h.url+"#cbp="+h.dataArray[h.current].url),a.isFunction(h.options.singlePageCallback)&&h.options.singlePageCallback.call(h,h.dataArray[h.current].url,h.dataArray[h.current].element)}},openSinglePageInline:function(c,d,e){var f,g,h,i,j=this;if(e=e||!1,j.fromOpen=e,j.storeBlocks=c,j.storeCurrentBlock=d,j.isOpen)return g=a(d).closest(".cbp-item").index(),void(j.dataArray[j.current].url!==d.getAttribute("href")||j.current!==g?j.cubeportfolio.singlePageInline.close("open",{blocks:c,currentBlock:d,fromOpen:!0}):j.close());if(j.isOpen=!0,j.stopEvents=!1,j.dataArray=[],j.current=null,f=d.getAttribute("href"),null===f)throw new Error("HEI! Your clicked element doesn't have a href attribute.");if(h=a(d).closest(".cbp-item")[0],c.each(function(a,b){h===b&&(j.current=a)}),j.dataArray[j.current]={url:f,element:d},i=a(j.dataArray[j.current].element).parents(".cbp-item").addClass("cbp-singlePageInline-active"),j.counterTotal=c.length,j.wrap.insertBefore(j.cubeportfolio.wrapper),"top"===j.options.singlePageInlinePosition)j.blocksToMove=c,j.top=0;else if("bottom"===j.options.singlePageInlinePosition)j.blocksToMove=a(),j.top=j.cubeportfolio.height;else if("above"===j.options.singlePageInlinePosition)j.top=a(c[j.current]).data("cbp").top,j.blocksToMove=a(),c.each(function(b,c){var d=a(c).data("cbp");d.top+d.height>=j.top&&(j.blocksToMove=j.blocksToMove.add(c))}),j.top=Math.max(j.top-j.options.gapHorizontal,0);else{var k=a(c[j.current]).data("cbp");j.top=k.top+k.height,j.blocksToMove=a(),c.each(function(b,c){var d=a(c).data("cbp");d.top+d.height>j.top&&(j.blocksToMove=j.blocksToMove.add(c))})}if(j.wrap[0].style.height=j.wrap.outerHeight(!0)+"px",j.deferredInline=a.Deferred(),j.options.singlePageInlineInFocus){j.scrollTop=a(b).scrollTop();var l=j.cubeportfolio.$obj.offset().top+j.top-100;j.scrollTop!==l?a("html,body").animate({scrollTop:l},350).promise().then(function(){j.resizeSinglePageInline(),j.deferredInline.resolve()}):(j.resizeSinglePageInline(),j.deferredInline.resolve())}else j.resizeSinglePageInline(),j.deferredInline.resolve();j.cubeportfolio.$obj.addClass("cbp-popup-singlePageInline-open"),j.wrap.css({top:j.top}),j.options.singlePageInlineDeeplinking&&(j.url=j.url.split("#cbpi=")[0],location.href=j.url+"#cbpi="+j.dataArray[j.current].url),a.isFunction(j.options.singlePageInlineCallback)&&j.options.singlePageInlineCallback.call(j,j.dataArray[j.current].url,j.dataArray[j.current].element)},resizeSinglePageInline:function(){var a=this;a.height=0===a.top||a.top===a.cubeportfolio.height?a.wrap.outerHeight(!0):a.wrap.outerHeight(!0)-a.options.gapHorizontal,a.storeBlocks.each(function(a,b){f["private"].modernBrowser?b.style[f["private"].transform]="":b.style.marginTop=""}),a.blocksToMove.each(function(b,c){f["private"].modernBrowser?c.style[f["private"].transform]="translate3d(0px, "+a.height+"px, 0)":c.style.marginTop=a.height+"px"}),a.cubeportfolio.obj.style.height=a.cubeportfolio.height+a.height+"px"},revertResizeSinglePageInline:function(){var b=this;b.deferredInline=a.Deferred(),b.storeBlocks.each(function(a,b){f["private"].modernBrowser?b.style[f["private"].transform]="":b.style.marginTop=""}),b.cubeportfolio.obj.style.height=b.cubeportfolio.height+"px"},appendScriptsToWrap:function(a){var b=this,d=0,e=function(f){var g=c.createElement("script"),h=f.src;g.type="text/javascript",g.readyState?g.onreadystatechange=function(){("loaded"==g.readyState||"complete"==g.readyState)&&(g.onreadystatechange=null,d++,a[d]&&e(a[d]))}:g.onload=function(){d++,a[d]&&e(a[d])},h?g.src=h:g.text=f.text,b.content[0].appendChild(g)};e(a[0])},updateSinglePage:function(b,c,d){var e,f=this;f.content.addClass("cbp-popup-content").removeClass("cbp-popup-content-basic"),d===!1&&f.content.removeClass("cbp-popup-content").addClass("cbp-popup-content-basic"),f.counter&&(e=a(f.getCounterMarkup(f.options.singlePageCounter,f.current+1,f.counterTotal)),f.counter.text(e.text())),f.content.html(b),c&&f.appendScriptsToWrap(c),f.cubeportfolio.$obj.trigger("updateSinglePageStart.cbp"),f.finishOpen--,f.finishOpen<=0&&f.updateSinglePageIsOpen.call(f)},updateSinglePageIsOpen:function(){var b,c=this;c.wrap.addClass("cbp-popup-ready"),c.wrap.removeClass("cbp-popup-loading"),b=c.content.find(".cbp-slider"),b?(b.find(".cbp-slider-item").addClass("cbp-item"),c.slider=b.cubeportfolio({layoutMode:"slider",mediaQueries:[{width:1,cols:1}],gapHorizontal:0,gapVertical:0,caption:"",coverRatio:""})):c.slider=null,("android"===f["private"].browser||"ios"===f["private"].browser)&&a("html").css({position:"fixed"}),c.cubeportfolio.$obj.trigger("updateSinglePageComplete.cbp")},updateSinglePageInline:function(a,b){var c=this;c.content.html(a),b&&c.appendScriptsToWrap(b),c.cubeportfolio.$obj.trigger("updateSinglePageInlineStart.cbp"),c.singlePageInlineIsOpen.call(c)},singlePageInlineIsOpen:function(){function a(){b.wrap.addClass("cbp-popup-singlePageInline-ready"),b.wrap[0].style.height="",b.resizeSinglePageInline(),b.cubeportfolio.$obj.trigger("updateSinglePageInlineComplete.cbp")}var b=this;b.cubeportfolio.loadImages(b.wrap,function(){var c=b.content.find(".cbp-slider");c.length?(c.find(".cbp-slider-item").addClass("cbp-item"),c.one("initComplete.cbp",function(){b.deferredInline.done(a)}),c.on("pluginResize.cbp",function(){b.deferredInline.done(a)}),b.slider=c.cubeportfolio({layoutMode:"slider",displayType:"default",mediaQueries:[{width:1,cols:1}],gapHorizontal:0,gapVertical:0,caption:"",coverRatio:""})):(b.slider=null,b.deferredInline.done(a))})},isImage:function(b){var c=this;new Image;c.tooggleLoading(!0),c.cubeportfolio.loadImages(a('<div><img src="'+b.src+'"></div>'),function(){c.updateImagesMarkup(b.src,b.title,c.getCounterMarkup(c.options.lightboxCounter,c.current+1,c.counterTotal)),c.tooggleLoading(!1)})},isVimeo:function(a){var b=this;b.updateVideoMarkup(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},isYoutube:function(a){var b=this;b.updateVideoMarkup(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},isTed:function(a){var b=this;b.updateVideoMarkup(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},isSoundCloud:function(a){var b=this;b.updateVideoMarkup(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},isSelfHostedVideo:function(a){var b=this;b.updateSelfHostedVideo(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},isSelfHostedAudio:function(a){var b=this;b.updateSelfHostedAudio(a.src,a.title,b.getCounterMarkup(b.options.lightboxCounter,b.current+1,b.counterTotal))},getCounterMarkup:function(a,b,c){if(!a.length)return"";var d={current:b,total:c};return a.replace(/\{\{current}}|\{\{total}}/gi,function(a){return d[a.slice(2,-2)]})},updateSelfHostedVideo:function(a,b,c){var d,e=this;e.wrap.addClass("cbp-popup-lightbox-isIframe");var f='<div class="cbp-popup-lightbox-iframe"><video controls="controls" height="auto" style="width: 100%">';for(d=0;d<a.length;d++)/(\.mp4)/i.test(a[d])?f+='<source src="'+a[d]+'" type="video/mp4">':/(\.ogg)|(\.ogv)/i.test(a[d])?f+='<source src="'+a[d]+'" type="video/ogg">':/(\.webm)/i.test(a[d])&&(f+='<source src="'+a[d]+'" type="video/webm">');f+='Your browser does not support the video tag.</video><div class="cbp-popup-lightbox-bottom">'+(b?'<div class="cbp-popup-lightbox-title">'+b+"</div>":"")+c+"</div></div>",e.content.html(f),e.wrap.addClass("cbp-popup-ready"),e.preloadNearbyImages()},updateSelfHostedAudio:function(a,b,c){var d=this;d.wrap.addClass("cbp-popup-lightbox-isIframe");var e='<div class="cbp-popup-lightbox-iframe"><div class="cbp-misc-video"><audio controls="controls" height="auto" style="width: 75%"><source src="'+a+'" type="audio/mpeg">Your browser does not support the audio tag.</audio></div><div class="cbp-popup-lightbox-bottom">'+(b?'<div class="cbp-popup-lightbox-title">'+b+"</div>":"")+c+"</div></div>";d.content.html(e),d.wrap.addClass("cbp-popup-ready"),d.preloadNearbyImages()},updateVideoMarkup:function(a,b,c){var d=this;d.wrap.addClass("cbp-popup-lightbox-isIframe");var e='<div class="cbp-popup-lightbox-iframe"><iframe src="'+a+'" frameborder="0" allowfullscreen scrolling="no"></iframe><div class="cbp-popup-lightbox-bottom">'+(b?'<div class="cbp-popup-lightbox-title">'+b+"</div>":"")+c+"</div></div>";d.content.html(e),d.wrap.addClass("cbp-popup-ready"),d.preloadNearbyImages()},updateImagesMarkup:function(a,b,c){var d=this;d.wrap.removeClass("cbp-popup-lightbox-isIframe");var e='<div class="cbp-popup-lightbox-figure"><img src="'+a+'" class="cbp-popup-lightbox-img" '+d.dataActionImg+' /><div class="cbp-popup-lightbox-bottom">'+(b?'<div class="cbp-popup-lightbox-title">'+b+"</div>":"")+c+"</div></div>";d.content.html(e),d.wrap.addClass("cbp-popup-ready"),d.resizeImage(),d.preloadNearbyImages()},next:function(){var a=this;a[a.type+"JumpTo"](1)},prev:function(){var a=this;a[a.type+"JumpTo"](-1)},lightboxJumpTo:function(a){var b,c=this;c.current=c.getIndex(c.current+a),b=c.dataArray[c.current],c[b.type](b)},singlePageJumpTo:function(b){var c=this;c.current=c.getIndex(c.current+b),a.isFunction(c.options.singlePageCallback)&&(c.resetWrap(),c.wrap.scrollTop(0),c.wrap.addClass("cbp-popup-loading"),c.options.singlePageCallback.call(c,c.dataArray[c.current].url,c.dataArray[c.current].element),c.options.singlePageDeeplinking&&(location.href=c.url+"#cbp="+c.dataArray[c.current].url))},resetWrap:function(){var a=this;"singlePage"===a.type&&a.options.singlePageDeeplinking&&(location.href=a.url+"#"),"singlePageInline"===a.type&&a.options.singlePageInlineDeeplinking&&(location.href=a.url+"#")},getIndex:function(a){var b=this;return a%=b.counterTotal,0>a&&(a=b.counterTotal+a),a},close:function(c,d){function e(){h.content.html(""),h.wrap.detach(),h.cubeportfolio.$obj.removeClass("cbp-popup-singlePageInline-open cbp-popup-singlePageInline-close"),"promise"===c&&a.isFunction(d.callback)&&d.callback.call(h.cubeportfolio),h.resetWrap()}function g(){h.options.singlePageInlineInFocus&&"promise"!==c?a("html,body").animate({scrollTop:h.scrollTop},350).promise().then(function(){e()}):e()}var h=this;h.isOpen=!1,"singlePageInline"===h.type?"open"===c?(h.wrap.removeClass("cbp-popup-singlePageInline-ready"),a(h.dataArray[h.current].element).closest(".cbp-item").removeClass("cbp-singlePageInline-active"),h.openSinglePageInline(d.blocks,d.currentBlock,d.fromOpen)):(h.height=0,h.revertResizeSinglePageInline(),h.wrap.removeClass("cbp-popup-singlePageInline-ready"),h.cubeportfolio.$obj.addClass("cbp-popup-singlePageInline-close"),h.cubeportfolio.$obj.find(".cbp-item").removeClass("cbp-singlePageInline-active"),f["private"].modernBrowser?h.wrap.one(f["private"].transitionend,function(){g()}):g()):"singlePage"===h.type?(h.resetWrap(),h.wrap.removeClass("cbp-popup-ready"),("android"===f["private"].browser||"ios"===f["private"].browser)&&(a("html").css({position:""}),h.navigationWrap.appendTo(h.wrap),h.navigationMobile.remove()),a(b).scrollTop(h.scrollTop),setTimeout(function(){h.stopScroll=!0,h.navigationWrap.css({top:h.wrap.scrollTop()}),h.wrap.removeClass("cbp-popup-singlePage-open cbp-popup-singlePage-sticky"),("ie8"===f["private"].browser||"ie9"===f["private"].browser)&&(h.content.html(""),h.wrap.detach(),a("html").css({overflow:"",paddingRight:"",position:""}),h.navigationWrap.removeAttr("style"))},0),h.wrap.one(f["private"].transitionend,function(){h.content.html(""),h.wrap.detach(),a("html").css({overflow:"",paddingRight:"",position:""}),h.navigationWrap.removeAttr("style")})):(h.originalStyle?a("html").attr("style",h.originalStyle):a("html").css({overflow:"",paddingRight:""}),a(b).scrollTop(h.scrollTop),h.content.html(""),h.wrap.detach())},tooggleLoading:function(a){var b=this;b.stopEvents=a,b.wrap[a?"addClass":"removeClass"]("cbp-popup-loading")},resizeImage:function(){if(this.isOpen){var c=a(b).height(),d=this.content.find("img"),e=parseInt(d.css("margin-top"),10)+parseInt(d.css("margin-bottom"),10);d.css("max-height",c-e+"px")}},preloadNearbyImages:function(){var a=[],b=this;a.push(b.getIndex(b.current+1)),a.push(b.getIndex(b.current+2)),a.push(b.getIndex(b.current+3)),a.push(b.getIndex(b.current-1)),a.push(b.getIndex(b.current-2)),a.push(b.getIndex(b.current-3));for(var c=a.length-1;c>=0;c--)"isImage"===b.dataArray[a[c]].type&&b.cubeportfolio.checkSrc(b.dataArray[a[c]].src)}},h=!1,i=!1;e.prototype.run=function(){var b=this,d=b.parent,e=a(c.body);d.lightbox=null,d.options.lightboxDelegate&&!h&&(h=!0,d.lightbox=Object.create(g),d.lightbox.init(d,"lightbox"),e.on("click.cbp",d.options.lightboxDelegate,function(c){c.preventDefault();var e=a(this),f=e.attr("data-cbp-lightbox"),g=b.detectScope(e),h=g.data("cubeportfolio"),i=[];h?h.blocksOn.each(function(b,c){
var e=a(c);e.not(".cbp-item-off")&&e.find(d.options.lightboxDelegate).each(function(b,c){f?a(c).attr("data-cbp-lightbox")===f&&i.push(c):i.push(c)})}):i=f?g.find(d.options.lightboxDelegate+"[data-cbp-lightbox="+f+"]"):g.find(d.options.lightboxDelegate),d.lightbox.openLightbox(i,e[0])})),d.singlePage=null,d.options.singlePageDelegate&&!i&&(i=!0,d.singlePage=Object.create(g),d.singlePage.init(d,"singlePage"),e.on("click.cbp",d.options.singlePageDelegate,function(c){c.preventDefault();var e=a(this),f=e.attr("data-cbp-singlePage"),g=b.detectScope(e),h=g.data("cubeportfolio"),i=[];h?h.blocksOn.each(function(b,c){var e=a(c);e.not(".cbp-item-off")&&e.find(d.options.singlePageDelegate).each(function(b,c){f?a(c).attr("data-cbp-singlePage")===f&&i.push(c):i.push(c)})}):i=f?g.find(d.options.singlePageDelegate+"[data-cbp-singlePage="+f+"]"):g.find(d.options.singlePageDelegate),d.singlePage.openSinglePage(i,e[0])})),d.singlePageInline=null,d.options.singlePageDelegate&&(d.singlePageInline=Object.create(g),d.singlePageInline.init(d,"singlePageInline"),d.$obj.on("click.cbp",d.options.singlePageInlineDelegate,function(a){a.preventDefault(),d.singlePageInline.openSinglePageInline(d.blocksOn,this)}))},e.prototype.detectScope=function(b){var d,e,f;return d=b.closest(".cbp-popup-singlePageInline"),d.length?(f=b.closest(".cbp",d[0]),f.length?f:d):(e=b.closest(".cbp-popup-singlePage"),e.length?(f=b.closest(".cbp",e[0]),f.length?f:e):(f=b.closest(".cbp"),f.length?f:a(c.body)))},e.prototype.destroy=function(){var b=this.parent;a(c.body).off("click.cbp"),h=!1,i=!1,b.lightbox&&b.lightbox.destroy(),b.singlePage&&b.singlePage.destroy(),b.singlePageInline&&b.singlePageInline.destroy()},f.plugins.popUp=function(a){return new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,c.searchInput=a(b.options.search),c.searchInput.each(function(b,c){var d=c.getAttribute("data-search");d||(d="*"),a.data(c,"searchData",{value:c.value,el:d})});var d=null;c.searchInput.on("keyup.cbp paste.cbp",function(b){b.preventDefault();var e=a(this);clearTimeout(d),d=setTimeout(function(){c.runEvent.call(c,e)},300)}),c.searchNothing=c.searchInput.siblings(".cbp-search-nothing").detach(),c.searchNothingHeight=null,c.searchNothingHTML=c.searchNothing.html(),c.searchInput.siblings(".cbp-search-icon").on("click.cbp",function(b){b.preventDefault(),c.runEvent.call(c,a(this).prev().val(""))})}var f=a.fn.cubeportfolio.constructor;e.prototype.runEvent=function(b){var c=this,d=b.val(),e=b.data("searchData"),f=new RegExp(d,"i");e.value===d||c.parent.isAnimating||(e.value=d,d.length>0?b.attr("value",d):b.removeAttr("value"),c.parent.$obj.cubeportfolio("filter",function(b){var g=b.filter(function(b,c){var d=a(c).find(e.el).text();return d.search(f)>-1?!0:void 0});if(0===g.length&&c.searchNothing.length){var h=c.searchNothingHTML.replace("{{query}}",d);c.searchNothing.html(h),c.searchNothing.appendTo(c.parent.$obj),null===c.searchNothingHeight&&(c.searchNothingHeight=c.searchNothing.outerHeight(!0)),c.parent.registerEvent("resizeMainContainer",function(){c.parent.height=c.parent.height+c.searchNothingHeight,c.parent.obj.style.height=c.parent.height+"px"},!0)}else c.searchNothing.detach();return g},function(){b.trigger("keyup.cbp")}))},e.prototype.destroy=function(){var b=this;b.searchInput.off(".cbp"),b.searchInput.next(".cbp-search-icon").off(".cbp"),b.searchInput.each(function(b,c){a.removeData(c)})},f.plugins.search=function(a){return""===a.options.search?null:new e(a)}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(b){var c=this;c.parent=b,c.options=a.extend({},f,c.parent.options.plugins.slider);var d=a(c.options.pagination);d.length>0&&(c.parent.customPagination=d,c.parent.customPaginationItems=d.children(),c.parent.customPaginationClass=c.options.paginationClass,c.parent.customPaginationItems.on("click.cbp",function(b){b.preventDefault(),b.stopImmediatePropagation(),b.stopPropagation(),c.parent.sliderStopEvents||c.parent.jumpToSlider(a(this))})),c.parent.registerEvent("gridAdjust",function(){c.sliderMarkup.call(c.parent),c.parent.registerEvent("gridAdjust",function(){c.updateSlider.call(c.parent)})},!0)}var f={pagination:"",paginationClass:"cbp-pagination-active"},g=a.fn.cubeportfolio.constructor;e.prototype.sliderMarkup=function(){var b=this;b.sliderStopEvents=!1,b.sliderActive=0,b.$obj.one("initComplete.cbp",function(){b.$obj.addClass("cbp-mode-slider")}),b.nav=a("<div/>",{"class":"cbp-nav"}),b.nav.on("click.cbp","[data-slider-action]",function(c){if(c.preventDefault(),c.stopImmediatePropagation(),c.stopPropagation(),!b.sliderStopEvents){var d=a(this),e=d.attr("data-slider-action");b[e+"Slider"]&&b[e+"Slider"](d)}}),b.options.showNavigation&&(b.controls=a("<div/>",{"class":"cbp-nav-controls"}),b.navPrev=a("<div/>",{"class":"cbp-nav-prev","data-slider-action":"prev"}).appendTo(b.controls),b.navNext=a("<div/>",{"class":"cbp-nav-next","data-slider-action":"next"}).appendTo(b.controls),b.controls.appendTo(b.nav)),b.options.showPagination&&(b.navPagination=a("<div/>",{"class":"cbp-nav-pagination"}).appendTo(b.nav)),(b.controls||b.navPagination)&&b.nav.appendTo(b.$obj),b.updateSliderPagination(),b.options.auto&&(b.options.autoPauseOnHover&&(b.mouseIsEntered=!1,b.$obj.on("mouseenter.cbp",function(a){b.mouseIsEntered=!0,b.stopSliderAuto()}).on("mouseleave.cbp",function(a){b.mouseIsEntered=!1,b.startSliderAuto()})),b.startSliderAuto()),b.options.drag&&g["private"].modernBrowser&&b.dragSlider()},e.prototype.updateSlider=function(){var a=this;a.updateSliderPosition(),a.updateSliderPagination()},e.prototype.destroy=function(){var a=this;a.parent.customPaginationItems.off(".cbp")},g.plugins.slider=function(a){return"slider"!==a.options.layoutMode?null:new e(a)}}(jQuery,window,document);
/*
 * @build  : 20-07-2013
 * @author : Ram swaroop
 * @site   : Compzets.com
 */
(function($){
      
    // defines various easing effects
    $.easing['jswing'] = $.easing['swing'];
    $.extend( $.easing,
    {
            def: 'easeOutQuad',
            swing: function (x, t, b, c, d) {
                    return $.easing[$.easing.def](x, t, b, c, d);
            },
            easeInQuad: function (x, t, b, c, d) {
                    return c*(t/=d)*t + b;
            },
            easeOutQuad: function (x, t, b, c, d) {
                    return -c *(t/=d)*(t-2) + b;
            },
            easeInOutQuad: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return c/2*t*t + b;
                    return -c/2 * ((--t)*(t-2) - 1) + b;
            },
            easeInCubic: function (x, t, b, c, d) {
                    return c*(t/=d)*t*t + b;
            },
            easeOutCubic: function (x, t, b, c, d) {
                    return c*((t=t/d-1)*t*t + 1) + b;
            },
            easeInOutCubic: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return c/2*t*t*t + b;
                    return c/2*((t-=2)*t*t + 2) + b;
            },
            easeInQuart: function (x, t, b, c, d) {
                    return c*(t/=d)*t*t*t + b;
            },
            easeOutQuart: function (x, t, b, c, d) {
                    return -c * ((t=t/d-1)*t*t*t - 1) + b;
            },
            easeInOutQuart: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
                    return -c/2 * ((t-=2)*t*t*t - 2) + b;
            },
            easeInQuint: function (x, t, b, c, d) {
                    return c*(t/=d)*t*t*t*t + b;
            },
            easeOutQuint: function (x, t, b, c, d) {
                    return c*((t=t/d-1)*t*t*t*t + 1) + b;
            },
            easeInOutQuint: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
                    return c/2*((t-=2)*t*t*t*t + 2) + b;
            },
            easeInSine: function (x, t, b, c, d) {
                    return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
            },
            easeOutSine: function (x, t, b, c, d) {
                    return c * Math.sin(t/d * (Math.PI/2)) + b;
            },
            easeInOutSine: function (x, t, b, c, d) {
                    return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
            },
            easeInExpo: function (x, t, b, c, d) {
                    return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
            },
            easeOutExpo: function (x, t, b, c, d) {
                    return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
            },
            easeInOutExpo: function (x, t, b, c, d) {
                    if (t==0) return b;
                    if (t==d) return b+c;
                    if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
                    return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
            },
            easeInCirc: function (x, t, b, c, d) {
                    return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
            },
            easeOutCirc: function (x, t, b, c, d) {
                    return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
            },
            easeInOutCirc: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
                    return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
            },
            easeInElastic: function (x, t, b, c, d) {
                    var s=1.70158;var p=0;var a=c;
                    if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                    if (a < Math.abs(c)) { a=c; var s=p/4; }
                    else var s = p/(2*Math.PI) * Math.asin (c/a);
                    return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
            },
            easeOutElastic: function (x, t, b, c, d) {
                    var s=1.70158;var p=0;var a=c;
                    if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                    if (a < Math.abs(c)) { a=c; var s=p/4; }
                    else var s = p/(2*Math.PI) * Math.asin (c/a);
                    return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
            },
            easeInOutElastic: function (x, t, b, c, d) {
                    var s=1.70158;var p=0;var a=c;
                    if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
                    if (a < Math.abs(c)) { a=c; var s=p/4; }
                    else var s = p/(2*Math.PI) * Math.asin (c/a);
                    if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
                    return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
            },
            easeInBack: function (x, t, b, c, d, s) {
                    if (s == undefined) s = 1.70158;
                    return c*(t/=d)*t*((s+1)*t - s) + b;
            },
            easeOutBack: function (x, t, b, c, d, s) {
                    if (s == undefined) s = 1.70158;
                    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
            },
            easeInOutBack: function (x, t, b, c, d, s) {
                    if (s == undefined) s = 1.70158; 
                    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
                    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
            },
            easeInBounce: function (x, t, b, c, d) {
                    return c - $.easing.easeOutBounce (x, d-t, 0, c, d) + b;
            },
            easeOutBounce: function (x, t, b, c, d) {
                    if ((t/=d) < (1/2.75)) {
                            return c*(7.5625*t*t) + b;
                    } else if (t < (2/2.75)) {
                            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
                    } else if (t < (2.5/2.75)) {
                            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
                    } else {
                            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
                    }
            },
            easeInOutBounce: function (x, t, b, c, d) {
                    if (t < d/2) return $.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
                    return $.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
            }
    });
    
    $.fn.animatescroll = function(options) {
        
        // fetches options
        var opts = $.extend({},$.fn.animatescroll.defaults,options);

        // make sure the callback is a function
        if (typeof opts.onScrollStart == 'function') {
            // brings the scope to the callback
            opts.onScrollStart.call(this);
        }
                
        if(opts.element == "html,body") {
            // Get the distance of particular id or class from top
            var offset = this.offset().top;
        
            // Scroll the page to the desired position
            $(opts.element).stop().animate({ scrollTop: offset - opts.padding}, opts.scrollSpeed, opts.easing);
        }
        else {
            // Scroll the element to the desired position
            $(opts.element).stop().animate({ scrollTop: this.offset().top - this.parent().offset().top + this.parent().scrollTop() - opts.padding}, opts.scrollSpeed, opts.easing);
        }

        setTimeout(function() {

            // make sure the callback is a function
            if (typeof opts.onScrollEnd == 'function') {
                // brings the scope to the callback
                opts.onScrollEnd.call(this);
            }
        }, opts.scrollSpeed);
    };
    
    // default options
    $.fn.animatescroll.defaults = {        
        easing:"swing",
        scrollSpeed:800,
        padding:0,
        element:"html,body"
    };   
    
}(jQuery));
/*!
 * parallax.js v1.4.2 (http://pixelcog.github.io/parallax.js/)
 * @copyright 2016 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */

;(function ( $, window, document, undefined ) {

  // Polyfill for requestAnimationFrame
  // via: https://gist.github.com/paulirish/1579671

  (function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
        || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
      window.requestAnimationFrame = function(callback) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };

    if (!window.cancelAnimationFrame)
      window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
      };
  }());


  // Parallax Constructor

  function Parallax(element, options) {
    var self = this;

    if (typeof options == 'object') {
      delete options.refresh;
      delete options.render;
      $.extend(this, options);
    }

    this.$element = $(element);

    if (!this.imageSrc && this.$element.is('img')) {
      this.imageSrc = this.$element.attr('src');
    }

    var positions = (this.position + '').toLowerCase().match(/\S+/g) || [];

    if (positions.length < 1) {
      positions.push('center');
    }
    if (positions.length == 1) {
      positions.push(positions[0]);
    }

    if (positions[0] == 'top' || positions[0] == 'bottom' || positions[1] == 'left' || positions[1] == 'right') {
      positions = [positions[1], positions[0]];
    }

    if (this.positionX != undefined) positions[0] = this.positionX.toLowerCase();
    if (this.positionY != undefined) positions[1] = this.positionY.toLowerCase();

    self.positionX = positions[0];
    self.positionY = positions[1];

    if (this.positionX != 'left' && this.positionX != 'right') {
      if (isNaN(parseInt(this.positionX))) {
        this.positionX = 'center';
      } else {
        this.positionX = parseInt(this.positionX);
      }
    }

    if (this.positionY != 'top' && this.positionY != 'bottom') {
      if (isNaN(parseInt(this.positionY))) {
        this.positionY = 'center';
      } else {
        this.positionY = parseInt(this.positionY);
      }
    }

    this.position =
      this.positionX + (isNaN(this.positionX)? '' : 'px') + ' ' +
      this.positionY + (isNaN(this.positionY)? '' : 'px');

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      if (this.imageSrc && this.iosFix && !this.$element.is('img')) {
        this.$element.css({
          backgroundImage: 'url(' + this.imageSrc + ')',
          backgroundSize: 'cover',
          backgroundPosition: this.position
        });
      }
      return this;
    }

    if (navigator.userAgent.match(/(Android)/)) {
      if (this.imageSrc && this.androidFix && !this.$element.is('img')) {
        this.$element.css({
          backgroundImage: 'url(' + this.imageSrc + ')',
          backgroundSize: 'cover',
          backgroundPosition: this.position
        });
      }
      return this;
    }

    this.$mirror = $('<div />').prependTo('body');

    var slider = this.$element.find('>.parallax-slider');
    var sliderExisted = false;

    if (slider.length == 0)
      this.$slider = $('<img />').prependTo(this.$mirror);
    else {
      this.$slider = slider.prependTo(this.$mirror)
      sliderExisted = true;
    }

    this.$mirror.addClass('parallax-mirror').css({
      visibility: 'hidden',
      zIndex: this.zIndex,
      position: 'fixed',
      top: 0,
      left: 0,
      overflow: 'hidden'
    });

    this.$slider.addClass('parallax-slider').one('load', function() {
      if (!self.naturalHeight || !self.naturalWidth) {
        self.naturalHeight = this.naturalHeight || this.height || 1;
        self.naturalWidth  = this.naturalWidth  || this.width  || 1;
      }
      self.aspectRatio = self.naturalWidth / self.naturalHeight;

      Parallax.isSetup || Parallax.setup();
      Parallax.sliders.push(self);
      Parallax.isFresh = false;
      Parallax.requestRender();
    });

    if (!sliderExisted)
      this.$slider[0].src = this.imageSrc;

    if (this.naturalHeight && this.naturalWidth || this.$slider[0].complete || slider.length > 0) {
      this.$slider.trigger('load');
    }

  };


  // Parallax Instance Methods

  $.extend(Parallax.prototype, {
    speed:    0.2,
    bleed:    0,
    zIndex:   -100,
    iosFix:   true,
    androidFix: true,
    position: 'center',
    overScrollFix: false,

    refresh: function() {
      this.boxWidth        = this.$element.outerWidth();
      this.boxHeight       = this.$element.outerHeight() + this.bleed * 2;
      this.boxOffsetTop    = this.$element.offset().top - this.bleed;
      this.boxOffsetLeft   = this.$element.offset().left;
      this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;

      var winHeight = Parallax.winHeight;
      var docHeight = Parallax.docHeight;
      var maxOffset = Math.min(this.boxOffsetTop, docHeight - winHeight);
      var minOffset = Math.max(this.boxOffsetTop + this.boxHeight - winHeight, 0);
      var imageHeightMin = this.boxHeight + (maxOffset - minOffset) * (1 - this.speed) | 0;
      var imageOffsetMin = (this.boxOffsetTop - maxOffset) * (1 - this.speed) | 0;

      if (imageHeightMin * this.aspectRatio >= this.boxWidth) {
        this.imageWidth    = imageHeightMin * this.aspectRatio | 0;
        this.imageHeight   = imageHeightMin;
        this.offsetBaseTop = imageOffsetMin;

        var margin = this.imageWidth - this.boxWidth;

        if (this.positionX == 'left') {
          this.offsetLeft = 0;
        } else if (this.positionX == 'right') {
          this.offsetLeft = - margin;
        } else if (!isNaN(this.positionX)) {
          this.offsetLeft = Math.max(this.positionX, - margin);
        } else {
          this.offsetLeft = - margin / 2 | 0;
        }
      } else {
        this.imageWidth    = this.boxWidth;
        this.imageHeight   = this.boxWidth / this.aspectRatio | 0;
        this.offsetLeft    = 0;

        var margin = this.imageHeight - imageHeightMin;

        if (this.positionY == 'top') {
          this.offsetBaseTop = imageOffsetMin;
        } else if (this.positionY == 'bottom') {
          this.offsetBaseTop = imageOffsetMin - margin;
        } else if (!isNaN(this.positionY)) {
          this.offsetBaseTop = imageOffsetMin + Math.max(this.positionY, - margin);
        } else {
          this.offsetBaseTop = imageOffsetMin - margin / 2 | 0;
        }
      }
    },

    render: function() {
      var scrollTop    = Parallax.scrollTop;
      var scrollLeft   = Parallax.scrollLeft;
      var overScroll   = this.overScrollFix ? Parallax.overScroll : 0;
      var scrollBottom = scrollTop + Parallax.winHeight;

      if (this.boxOffsetBottom > scrollTop && this.boxOffsetTop <= scrollBottom) {
        this.visibility = 'visible';
        this.mirrorTop = this.boxOffsetTop  - scrollTop;
        this.mirrorLeft = this.boxOffsetLeft - scrollLeft;
        this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed);
      } else {
        this.visibility = 'hidden';
      }

      this.$mirror.css({
        transform: 'translate3d(0px, 0px, 0px)',
        visibility: this.visibility,
        top: this.mirrorTop - overScroll,
        left: this.mirrorLeft,
        height: this.boxHeight,
        width: this.boxWidth
      });

      this.$slider.css({
        transform: 'translate3d(0px, 0px, 0px)',
        position: 'absolute',
        top: this.offsetTop,
        left: this.offsetLeft,
        height: this.imageHeight,
        width: this.imageWidth,
        maxWidth: 'none'
      });
    }
  });


  // Parallax Static Methods

  $.extend(Parallax, {
    scrollTop:    0,
    scrollLeft:   0,
    winHeight:    0,
    winWidth:     0,
    docHeight:    1 << 30,
    docWidth:     1 << 30,
    sliders:      [],
    isReady:      false,
    isFresh:      false,
    isBusy:       false,

    setup: function() {
      if (this.isReady) return;

      var $doc = $(document), $win = $(window);

      var loadDimensions = function() {
        Parallax.winHeight = $win.height();
        Parallax.winWidth  = $win.width();
        Parallax.docHeight = $doc.height();
        Parallax.docWidth  = $doc.width();
      };

      var loadScrollPosition = function() {
        var winScrollTop  = $win.scrollTop();
        var scrollTopMax  = Parallax.docHeight - Parallax.winHeight;
        var scrollLeftMax = Parallax.docWidth  - Parallax.winWidth;
        Parallax.scrollTop  = Math.max(0, Math.min(scrollTopMax,  winScrollTop));
        Parallax.scrollLeft = Math.max(0, Math.min(scrollLeftMax, $win.scrollLeft()));
        Parallax.overScroll = Math.max(winScrollTop - scrollTopMax, Math.min(winScrollTop, 0));
      };

      $win.on('resize.px.parallax load.px.parallax', function() {
          loadDimensions();
          Parallax.isFresh = false;
          Parallax.requestRender();
        })
        .on('scroll.px.parallax load.px.parallax', function() {
          loadScrollPosition();
          Parallax.requestRender();
        });

      loadDimensions();
      loadScrollPosition();

      this.isReady = true;
    },

    configure: function(options) {
      if (typeof options == 'object') {
        delete options.refresh;
        delete options.render;
        $.extend(this.prototype, options);
      }
    },

    refresh: function() {
      $.each(this.sliders, function(){ this.refresh() });
      this.isFresh = true;
    },

    render: function() {
      this.isFresh || this.refresh();
      $.each(this.sliders, function(){ this.render() });
    },

    requestRender: function() {
      var self = this;

      if (!this.isBusy) {
        this.isBusy = true;
        window.requestAnimationFrame(function() {
          self.render();
          self.isBusy = false;
        });
      }
    },
    destroy: function(el){
      var i,
          parallaxElement = $(el).data('px.parallax');
      parallaxElement.$mirror.remove();
      for(i=0; i < this.sliders.length; i+=1){
        if(this.sliders[i] == parallaxElement){
          this.sliders.splice(i, 1);
        }
      }
      $(el).data('px.parallax', false);
      if(this.sliders.length === 0){
        $(window).off('scroll.px.parallax resize.px.parallax load.px.parallax');
        this.isReady = false;
        Parallax.isSetup = false;
      }
    }
  });


  // Parallax Plugin Definition

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var options = typeof option == 'object' && option;

      if (this == window || this == document || $this.is('body')) {
        Parallax.configure(options);
      }
      else if (!$this.data('px.parallax')) {
        options = $.extend({}, $this.data(), options);
        $this.data('px.parallax', new Parallax(this, options));
      }
      else if (typeof option == 'object')
      {
        $.extend($this.data('px.parallax'), options);
      }
      if (typeof option == 'string') {
        if(option == 'destroy'){
            Parallax['destroy'](this);
        }else{
          Parallax[option]();
        }
      }
    })
  };

  var old = $.fn.parallax;

  $.fn.parallax             = Plugin;
  $.fn.parallax.Constructor = Parallax;


  // Parallax No Conflict

  $.fn.parallax.noConflict = function () {
    $.fn.parallax = old;
    return this;
  };


  // Parallax Data-API

  $(document).on('ready.px.parallax.data-api', function () {
    $('[data-parallax="scroll"]').parallax();
  });

}(jQuery, window, document));
/*!
 * parallax.js v1.4.2 (http://pixelcog.github.io/parallax.js/)
 * @copyright 2016 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */
!function(t,i,e,s){function o(i,e){var h=this;"object"==typeof e&&(delete e.refresh,delete e.render,t.extend(this,e)),this.$element=t(i),!this.imageSrc&&this.$element.is("img")&&(this.imageSrc=this.$element.attr("src"));var r=(this.position+"").toLowerCase().match(/\S+/g)||[];if(r.length<1&&r.push("center"),1==r.length&&r.push(r[0]),("top"==r[0]||"bottom"==r[0]||"left"==r[1]||"right"==r[1])&&(r=[r[1],r[0]]),this.positionX!=s&&(r[0]=this.positionX.toLowerCase()),this.positionY!=s&&(r[1]=this.positionY.toLowerCase()),h.positionX=r[0],h.positionY=r[1],"left"!=this.positionX&&"right"!=this.positionX&&(this.positionX=isNaN(parseInt(this.positionX))?"center":parseInt(this.positionX)),"top"!=this.positionY&&"bottom"!=this.positionY&&(this.positionY=isNaN(parseInt(this.positionY))?"center":parseInt(this.positionY)),this.position=this.positionX+(isNaN(this.positionX)?"":"px")+" "+this.positionY+(isNaN(this.positionY)?"":"px"),navigator.userAgent.match(/(iPod|iPhone|iPad)/))return this.imageSrc&&this.iosFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;if(navigator.userAgent.match(/(Android)/))return this.imageSrc&&this.androidFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;this.$mirror=t("<div />").prependTo("body");var a=this.$element.find(">.parallax-slider"),n=!1;0==a.length?this.$slider=t("<img />").prependTo(this.$mirror):(this.$slider=a.prependTo(this.$mirror),n=!0),this.$mirror.addClass("parallax-mirror").css({visibility:"hidden",zIndex:this.zIndex,position:"fixed",top:0,left:0,overflow:"hidden"}),this.$slider.addClass("parallax-slider").one("load",function(){h.naturalHeight&&h.naturalWidth||(h.naturalHeight=this.naturalHeight||this.height||1,h.naturalWidth=this.naturalWidth||this.width||1),h.aspectRatio=h.naturalWidth/h.naturalHeight,o.isSetup||o.setup(),o.sliders.push(h),o.isFresh=!1,o.requestRender()}),n||(this.$slider[0].src=this.imageSrc),(this.naturalHeight&&this.naturalWidth||this.$slider[0].complete||a.length>0)&&this.$slider.trigger("load")}function h(s){return this.each(function(){var h=t(this),r="object"==typeof s&&s;this==i||this==e||h.is("body")?o.configure(r):h.data("px.parallax")?"object"==typeof s&&t.extend(h.data("px.parallax"),r):(r=t.extend({},h.data(),r),h.data("px.parallax",new o(this,r))),"string"==typeof s&&("destroy"==s?o.destroy(this):o[s]())})}!function(){for(var t=0,e=["ms","moz","webkit","o"],s=0;s<e.length&&!i.requestAnimationFrame;++s)i.requestAnimationFrame=i[e[s]+"RequestAnimationFrame"],i.cancelAnimationFrame=i[e[s]+"CancelAnimationFrame"]||i[e[s]+"CancelRequestAnimationFrame"];i.requestAnimationFrame||(i.requestAnimationFrame=function(e){var s=(new Date).getTime(),o=Math.max(0,16-(s-t)),h=i.setTimeout(function(){e(s+o)},o);return t=s+o,h}),i.cancelAnimationFrame||(i.cancelAnimationFrame=function(t){clearTimeout(t)})}(),t.extend(o.prototype,{speed:.2,bleed:0,zIndex:-100,iosFix:!0,androidFix:!0,position:"center",overScrollFix:!1,refresh:function(){this.boxWidth=this.$element.outerWidth(),this.boxHeight=this.$element.outerHeight()+2*this.bleed,this.boxOffsetTop=this.$element.offset().top-this.bleed,this.boxOffsetLeft=this.$element.offset().left,this.boxOffsetBottom=this.boxOffsetTop+this.boxHeight;var t=o.winHeight,i=o.docHeight,e=Math.min(this.boxOffsetTop,i-t),s=Math.max(this.boxOffsetTop+this.boxHeight-t,0),h=this.boxHeight+(e-s)*(1-this.speed)|0,r=(this.boxOffsetTop-e)*(1-this.speed)|0;if(h*this.aspectRatio>=this.boxWidth){this.imageWidth=h*this.aspectRatio|0,this.imageHeight=h,this.offsetBaseTop=r;var a=this.imageWidth-this.boxWidth;this.offsetLeft="left"==this.positionX?0:"right"==this.positionX?-a:isNaN(this.positionX)?-a/2|0:Math.max(this.positionX,-a)}else{this.imageWidth=this.boxWidth,this.imageHeight=this.boxWidth/this.aspectRatio|0,this.offsetLeft=0;var a=this.imageHeight-h;this.offsetBaseTop="top"==this.positionY?r:"bottom"==this.positionY?r-a:isNaN(this.positionY)?r-a/2|0:r+Math.max(this.positionY,-a)}},render:function(){var t=o.scrollTop,i=o.scrollLeft,e=this.overScrollFix?o.overScroll:0,s=t+o.winHeight;this.boxOffsetBottom>t&&this.boxOffsetTop<=s?(this.visibility="visible",this.mirrorTop=this.boxOffsetTop-t,this.mirrorLeft=this.boxOffsetLeft-i,this.offsetTop=this.offsetBaseTop-this.mirrorTop*(1-this.speed)):this.visibility="hidden",this.$mirror.css({transform:"translate3d(0px, 0px, 0px)",visibility:this.visibility,top:this.mirrorTop-e,left:this.mirrorLeft,height:this.boxHeight,width:this.boxWidth}),this.$slider.css({transform:"translate3d(0px, 0px, 0px)",position:"absolute",top:this.offsetTop,left:this.offsetLeft,height:this.imageHeight,width:this.imageWidth,maxWidth:"none"})}}),t.extend(o,{scrollTop:0,scrollLeft:0,winHeight:0,winWidth:0,docHeight:1<<30,docWidth:1<<30,sliders:[],isReady:!1,isFresh:!1,isBusy:!1,setup:function(){if(!this.isReady){var s=t(e),h=t(i),r=function(){o.winHeight=h.height(),o.winWidth=h.width(),o.docHeight=s.height(),o.docWidth=s.width()},a=function(){var t=h.scrollTop(),i=o.docHeight-o.winHeight,e=o.docWidth-o.winWidth;o.scrollTop=Math.max(0,Math.min(i,t)),o.scrollLeft=Math.max(0,Math.min(e,h.scrollLeft())),o.overScroll=Math.max(t-i,Math.min(t,0))};h.on("resize.px.parallax load.px.parallax",function(){r(),o.isFresh=!1,o.requestRender()}).on("scroll.px.parallax load.px.parallax",function(){a(),o.requestRender()}),r(),a(),this.isReady=!0}},configure:function(i){"object"==typeof i&&(delete i.refresh,delete i.render,t.extend(this.prototype,i))},refresh:function(){t.each(this.sliders,function(){this.refresh()}),this.isFresh=!0},render:function(){this.isFresh||this.refresh(),t.each(this.sliders,function(){this.render()})},requestRender:function(){var t=this;this.isBusy||(this.isBusy=!0,i.requestAnimationFrame(function(){t.render(),t.isBusy=!1}))},destroy:function(e){var s,h=t(e).data("px.parallax");for(h.$mirror.remove(),s=0;s<this.sliders.length;s+=1)this.sliders[s]==h&&this.sliders.splice(s,1);t(e).data("px.parallax",!1),0===this.sliders.length&&(t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"),this.isReady=!1,o.isSetup=!1)}});var r=t.fn.parallax;t.fn.parallax=h,t.fn.parallax.Constructor=o,t.fn.parallax.noConflict=function(){return t.fn.parallax=r,this},t(e).on("ready.px.parallax.data-api",function(){t('[data-parallax="scroll"]').parallax()})}(jQuery,window,document);
/**
 * Owl Carousel v2.2.1
 * Copyright 2013-2017 David Deutsch
 * Licensed under  ()
 */
/**
 * Owl carousel
 * @version 2.1.6
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 * @todo Lazy Load Icon
 * @todo prevent animationend bubling
 * @todo itemsScaleUp
 * @todo Test Zepto
 * @todo stagePadding calculate wrong active classes
 */
;(function($, window, document, undefined) {

	/**
	 * Creates a carousel.
	 * @class The Owl Carousel.
	 * @public
	 * @param {HTMLElement|jQuery} element - The element to create the carousel for.
	 * @param {Object} [options] - The options
	 */
	function Owl(element, options) {

		/**
		 * Current settings for the carousel.
		 * @public
		 */
		this.settings = null;

		/**
		 * Current options set by the caller including defaults.
		 * @public
		 */
		this.options = $.extend({}, Owl.Defaults, options);

		/**
		 * Plugin element.
		 * @public
		 */
		this.$element = $(element);

		/**
		 * Proxied event handlers.
		 * @protected
		 */
		this._handlers = {};

		/**
		 * References to the running plugins of this carousel.
		 * @protected
		 */
		this._plugins = {};

		/**
		 * Currently suppressed events to prevent them from beeing retriggered.
		 * @protected
		 */
		this._supress = {};

		/**
		 * Absolute current position.
		 * @protected
		 */
		this._current = null;

		/**
		 * Animation speed in milliseconds.
		 * @protected
		 */
		this._speed = null;

		/**
		 * Coordinates of all items in pixel.
		 * @todo The name of this member is missleading.
		 * @protected
		 */
		this._coordinates = [];

		/**
		 * Current breakpoint.
		 * @todo Real media queries would be nice.
		 * @protected
		 */
		this._breakpoint = null;

		/**
		 * Current width of the plugin element.
		 */
		this._width = null;

		/**
		 * All real items.
		 * @protected
		 */
		this._items = [];

		/**
		 * All cloned items.
		 * @protected
		 */
		this._clones = [];

		/**
		 * Merge values of all items.
		 * @todo Maybe this could be part of a plugin.
		 * @protected
		 */
		this._mergers = [];

		/**
		 * Widths of all items.
		 */
		this._widths = [];

		/**
		 * Invalidated parts within the update process.
		 * @protected
		 */
		this._invalidated = {};

		/**
		 * Ordered list of workers for the update process.
		 * @protected
		 */
		this._pipe = [];

		/**
		 * Current state information for the drag operation.
		 * @todo #261
		 * @protected
		 */
		this._drag = {
			time: null,
			target: null,
			pointer: null,
			stage: {
				start: null,
				current: null
			},
			direction: null
		};

		/**
		 * Current state information and their tags.
		 * @type {Object}
		 * @protected
		 */
		this._states = {
			current: {},
			tags: {
				'initializing': [ 'busy' ],
				'animating': [ 'busy' ],
				'dragging': [ 'interacting' ]
			}
		};

		$.each([ 'onResize', 'onThrottledResize' ], $.proxy(function(i, handler) {
			this._handlers[handler] = $.proxy(this[handler], this);
		}, this));

		$.each(Owl.Plugins, $.proxy(function(key, plugin) {
			this._plugins[key.charAt(0).toLowerCase() + key.slice(1)]
				= new plugin(this);
		}, this));

		$.each(Owl.Workers, $.proxy(function(priority, worker) {
			this._pipe.push({
				'filter': worker.filter,
				'run': $.proxy(worker.run, this)
			});
		}, this));

		this.setup();
		this.initialize();
	}

	/**
	 * Default options for the carousel.
	 * @public
	 */
	Owl.Defaults = {
		items: 3,
		loop: false,
		center: false,
		rewind: false,

		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		freeDrag: false,

		margin: 0,
		stagePadding: 0,

		merge: false,
		mergeFit: true,
		autoWidth: false,

		startPosition: 0,
		rtl: false,

		smartSpeed: 250,
		fluidSpeed: false,
		dragEndSpeed: false,

		responsive: {},
		responsiveRefreshRate: 200,
		responsiveBaseElement: window,

		fallbackEasing: 'swing',

		info: false,

		nestedItemSelector: false,
		itemElement: 'div',
		stageElement: 'div',

		refreshClass: 'owl-refresh',
		loadedClass: 'owl-loaded',
		loadingClass: 'owl-loading',
		rtlClass: 'owl-rtl',
		responsiveClass: 'owl-responsive',
		dragClass: 'owl-drag',
		itemClass: 'owl-item',
		stageClass: 'owl-stage',
		stageOuterClass: 'owl-stage-outer',
		grabClass: 'owl-grab'
	};

	/**
	 * Enumeration for width.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Width = {
		Default: 'default',
		Inner: 'inner',
		Outer: 'outer'
	};

	/**
	 * Enumeration for types.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Type = {
		Event: 'event',
		State: 'state'
	};

	/**
	 * Contains all registered plugins.
	 * @public
	 */
	Owl.Plugins = {};

	/**
	 * List of workers involved in the update process.
	 */
	Owl.Workers = [ {
		filter: [ 'width', 'settings' ],
		run: function() {
			this._width = this.$element.width();
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			cache.current = this._items && this._items[this.relative(this._current)];
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			this.$stage.children('.cloned').remove();
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var margin = this.settings.margin || '',
				grid = !this.settings.autoWidth,
				rtl = this.settings.rtl,
				css = {
					'width': 'auto',
					'margin-left': rtl ? margin : '',
					'margin-right': rtl ? '' : margin
				};

			!grid && this.$stage.children().css(css);

			cache.css = css;
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
				merge = null,
				iterator = this._items.length,
				grid = !this.settings.autoWidth,
				widths = [];

			cache.items = {
				merge: false,
				width: width
			};

			while (iterator--) {
				merge = this._mergers[iterator];
				merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;

				cache.items.merge = merge > 1 || cache.items.merge;

				widths[iterator] = !grid ? this._items[iterator].width() : width * merge;
			}

			this._widths = widths;
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			var clones = [],
				items = this._items,
				settings = this.settings,
				// TODO: Should be computed from number of min width items in stage
				view = Math.max(settings.items * 2, 4),
				size = Math.ceil(items.length / 2) * 2,
				repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
				append = '',
				prepend = '';

			repeat /= 2;

			while (repeat--) {
				// Switch to only using appended clones
				clones.push(this.normalize(clones.length / 2, true));
				append = append + items[clones[clones.length - 1]][0].outerHTML;
				clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
				prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
			}

			this._clones = clones;

			$(append).addClass('cloned').appendTo(this.$stage);
			$(prepend).addClass('cloned').prependTo(this.$stage);
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				size = this._clones.length + this._items.length,
				iterator = -1,
				previous = 0,
				current = 0,
				coordinates = [];

			while (++iterator < size) {
				previous = coordinates[iterator - 1] || 0;
				current = this._widths[this.relative(iterator)] + this.settings.margin;
				coordinates.push(previous + current * rtl);
			}

			this._coordinates = coordinates;
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function() {
			var padding = this.settings.stagePadding,
				coordinates = this._coordinates,
				css = {
					'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
					'padding-left': padding || '',
					'padding-right': padding || ''
				};

			this.$stage.css(css);
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var iterator = this._coordinates.length,
				grid = !this.settings.autoWidth,
				items = this.$stage.children();

			if (grid && cache.items.merge) {
				while (iterator--) {
					cache.css.width = this._widths[this.relative(iterator)];
					items.eq(iterator).css(cache.css);
				}
			} else if (grid) {
				cache.css.width = cache.items.width;
				items.css(cache.css);
			}
		}
	}, {
		filter: [ 'items' ],
		run: function() {
			this._coordinates.length < 1 && this.$stage.removeAttr('style');
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
			cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
			this.reset(cache.current);
		}
	}, {
		filter: [ 'position' ],
		run: function() {
			this.animate(this.coordinates(this._current));
		}
	}, {
		filter: [ 'width', 'position', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				padding = this.settings.stagePadding * 2,
				begin = this.coordinates(this.current()) + padding,
				end = begin + this.width() * rtl,
				inner, outer, matches = [], i, n;

			for (i = 0, n = this._coordinates.length; i < n; i++) {
				inner = this._coordinates[i - 1] || 0;
				outer = Math.abs(this._coordinates[i]) + padding * rtl;

				if ((this.op(inner, '<=', begin) && (this.op(inner, '>', end)))
					|| (this.op(outer, '<', begin) && this.op(outer, '>', end))) {
					matches.push(i);
				}
			}

			this.$stage.children('.active').removeClass('active');
			this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');

			if (this.settings.center) {
				this.$stage.children('.center').removeClass('center');
				this.$stage.children().eq(this.current()).addClass('center');
			}
		}
	} ];

	/**
	 * Initializes the carousel.
	 * @protected
	 */
	Owl.prototype.initialize = function() {
		this.enter('initializing');
		this.trigger('initialize');

		this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);

		if (this.settings.autoWidth && !this.is('pre-loading')) {
			var imgs, nestedSelector, width;
			imgs = this.$element.find('img');
			nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
			width = this.$element.children(nestedSelector).width();

			if (imgs.length && width <= 0) {
				this.preloadAutoWidthImages(imgs);
			}
		}

		this.$element.addClass(this.options.loadingClass);

		// create stage
		this.$stage = $('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>')
			.wrap('<div class="' + this.settings.stageOuterClass + '"/>');

		// append stage
		this.$element.append(this.$stage.parent());

		// append content
		this.replace(this.$element.children().not(this.$stage.parent()));

		// check visibility
		if (this.$element.is(':visible')) {
			// update view
			this.refresh();
		} else {
			// invalidate width
			this.invalidate('width');
		}

		this.$element
			.removeClass(this.options.loadingClass)
			.addClass(this.options.loadedClass);

		// register event handlers
		this.registerEventHandlers();

		this.leave('initializing');
		this.trigger('initialized');
	};

	/**
	 * Setups the current settings.
	 * @todo Remove responsive classes. Why should adaptive designs be brought into IE8?
	 * @todo Support for media queries by using `matchMedia` would be nice.
	 * @public
	 */
	Owl.prototype.setup = function() {
		var viewport = this.viewport(),
			overwrites = this.options.responsive,
			match = -1,
			settings = null;

		if (!overwrites) {
			settings = $.extend({}, this.options);
		} else {
			$.each(overwrites, function(breakpoint) {
				if (breakpoint <= viewport && breakpoint > match) {
					match = Number(breakpoint);
				}
			});

			settings = $.extend({}, this.options, overwrites[match]);
			if (typeof settings.stagePadding === 'function') {
				settings.stagePadding = settings.stagePadding();
			}
			delete settings.responsive;

			// responsive class
			if (settings.responsiveClass) {
				this.$element.attr('class',
					this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match)
				);
			}
		}

		this.trigger('change', { property: { name: 'settings', value: settings } });
		this._breakpoint = match;
		this.settings = settings;
		this.invalidate('settings');
		this.trigger('changed', { property: { name: 'settings', value: this.settings } });
	};

	/**
	 * Updates option logic if necessery.
	 * @protected
	 */
	Owl.prototype.optionsLogic = function() {
		if (this.settings.autoWidth) {
			this.settings.stagePadding = false;
			this.settings.merge = false;
		}
	};

	/**
	 * Prepares an item before add.
	 * @todo Rename event parameter `content` to `item`.
	 * @protected
	 * @returns {jQuery|HTMLElement} - The item container.
	 */
	Owl.prototype.prepare = function(item) {
		var event = this.trigger('prepare', { content: item });

		if (!event.data) {
			event.data = $('<' + this.settings.itemElement + '/>')
				.addClass(this.options.itemClass).append(item)
		}

		this.trigger('prepared', { content: event.data });

		return event.data;
	};

	/**
	 * Updates the view.
	 * @public
	 */
	Owl.prototype.update = function() {
		var i = 0,
			n = this._pipe.length,
			filter = $.proxy(function(p) { return this[p] }, this._invalidated),
			cache = {};

		while (i < n) {
			if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
				this._pipe[i].run(cache);
			}
			i++;
		}

		this._invalidated = {};

		!this.is('valid') && this.enter('valid');
	};

	/**
	 * Gets the width of the view.
	 * @public
	 * @param {Owl.Width} [dimension=Owl.Width.Default] - The dimension to return.
	 * @returns {Number} - The width of the view in pixel.
	 */
	Owl.prototype.width = function(dimension) {
		dimension = dimension || Owl.Width.Default;
		switch (dimension) {
			case Owl.Width.Inner:
			case Owl.Width.Outer:
				return this._width;
			default:
				return this._width - this.settings.stagePadding * 2 + this.settings.margin;
		}
	};

	/**
	 * Refreshes the carousel primarily for adaptive purposes.
	 * @public
	 */
	Owl.prototype.refresh = function() {
		this.enter('refreshing');
		this.trigger('refresh');

		this.setup();

		this.optionsLogic();

		this.$element.addClass(this.options.refreshClass);

		this.update();

		this.$element.removeClass(this.options.refreshClass);

		this.leave('refreshing');
		this.trigger('refreshed');
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onThrottledResize = function() {
		window.clearTimeout(this.resizeTimer);
		this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onResize = function() {
		if (!this._items.length) {
			return false;
		}

		if (this._width === this.$element.width()) {
			return false;
		}

		if (!this.$element.is(':visible')) {
			return false;
		}

		this.enter('resizing');

		if (this.trigger('resize').isDefaultPrevented()) {
			this.leave('resizing');
			return false;
		}

		this.invalidate('width');

		this.refresh();

		this.leave('resizing');
		this.trigger('resized');
	};

	/**
	 * Registers event handlers.
	 * @todo Check `msPointerEnabled`
	 * @todo #261
	 * @protected
	 */
	Owl.prototype.registerEventHandlers = function() {
		if ($.support.transition) {
			this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
		}

		if (this.settings.responsive !== false) {
			this.on(window, 'resize', this._handlers.onThrottledResize);
		}

		if (this.settings.mouseDrag) {
			this.$element.addClass(this.options.dragClass);
			this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('dragstart.owl.core selectstart.owl.core', function() { return false });
		}

		if (this.settings.touchDrag){
			this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
		}
	};

	/**
	 * Handles `touchstart` and `mousedown` events.
	 * @todo Horizontal swipe threshold as option
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragStart = function(event) {
		var stage = null;

		if (event.which === 3) {
			return;
		}

		if ($.support.transform) {
			stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
			stage = {
				x: stage[stage.length === 16 ? 12 : 4],
				y: stage[stage.length === 16 ? 13 : 5]
			};
		} else {
			stage = this.$stage.position();
			stage = {
				x: this.settings.rtl ?
					stage.left + this.$stage.width() - this.width() + this.settings.margin :
					stage.left,
				y: stage.top
			};
		}

		if (this.is('animating')) {
			$.support.transform ? this.animate(stage.x) : this.$stage.stop()
			this.invalidate('position');
		}

		this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');

		this.speed(0);

		this._drag.time = new Date().getTime();
		this._drag.target = $(event.target);
		this._drag.stage.start = stage;
		this._drag.stage.current = stage;
		this._drag.pointer = this.pointer(event);

		$(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));

		$(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function(event) {
			var delta = this.difference(this._drag.pointer, this.pointer(event));

			$(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));

			if (Math.abs(delta.x) < Math.abs(delta.y) && this.is('valid')) {
				return;
			}

			event.preventDefault();

			this.enter('dragging');
			this.trigger('drag');
		}, this));
	};

	/**
	 * Handles the `touchmove` and `mousemove` events.
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragMove = function(event) {
		var minimum = null,
			maximum = null,
			pull = null,
			delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this.difference(this._drag.stage.start, delta);

		if (!this.is('dragging')) {
			return;
		}

		event.preventDefault();

		if (this.settings.loop) {
			minimum = this.coordinates(this.minimum());
			maximum = this.coordinates(this.maximum() + 1) - minimum;
			stage.x = (((stage.x - minimum) % maximum + maximum) % maximum) + minimum;
		} else {
			minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
			maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
			pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0;
			stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
		}

		this._drag.stage.current = stage;

		this.animate(stage.x);
	};

	/**
	 * Handles the `touchend` and `mouseup` events.
	 * @todo #261
	 * @todo Threshold for click event
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragEnd = function(event) {
		var delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this._drag.stage.current,
			direction = delta.x > 0 ^ this.settings.rtl ? 'left' : 'right';

		$(document).off('.owl.core');

		this.$element.removeClass(this.options.grabClass);

		if (delta.x !== 0 && this.is('dragging') || !this.is('valid')) {
			this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
			this.current(this.closest(stage.x, delta.x !== 0 ? direction : this._drag.direction));
			this.invalidate('position');
			this.update();

			this._drag.direction = direction;

			if (Math.abs(delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
				this._drag.target.one('click.owl.core', function() { return false; });
			}
		}

		if (!this.is('dragging')) {
			return;
		}

		this.leave('dragging');
		this.trigger('dragged');
	};

	/**
	 * Gets absolute position of the closest item for a coordinate.
	 * @todo Setting `freeDrag` makes `closest` not reusable. See #165.
	 * @protected
	 * @param {Number} coordinate - The coordinate in pixel.
	 * @param {String} direction - The direction to check for the closest item. Ether `left` or `right`.
	 * @return {Number} - The absolute position of the closest item.
	 */
	Owl.prototype.closest = function(coordinate, direction) {
		var position = -1,
			pull = 30,
			width = this.width(),
			coordinates = this.coordinates();

		if (!this.settings.freeDrag) {
			// check closest item
			$.each(coordinates, $.proxy(function(index, value) {
				// on a left pull, check on current index
				if (direction === 'left' && coordinate > value - pull && coordinate < value + pull) {
					position = index;
				// on a right pull, check on previous index
				// to do so, subtract width from value and set position = index + 1
				} else if (direction === 'right' && coordinate > value - width - pull && coordinate < value - width + pull) {
					position = index + 1;
				} else if (this.op(coordinate, '<', value)
					&& this.op(coordinate, '>', coordinates[index + 1] || value - width)) {
					position = direction === 'left' ? index + 1 : index;
				}
				return position === -1;
			}, this));
		}

		if (!this.settings.loop) {
			// non loop boundries
			if (this.op(coordinate, '>', coordinates[this.minimum()])) {
				position = coordinate = this.minimum();
			} else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
				position = coordinate = this.maximum();
			}
		}

		return position;
	};

	/**
	 * Animates the stage.
	 * @todo #270
	 * @public
	 * @param {Number} coordinate - The coordinate in pixels.
	 */
	Owl.prototype.animate = function(coordinate) {
		var animate = this.speed() > 0;

		this.is('animating') && this.onTransitionEnd();

		if (animate) {
			this.enter('animating');
			this.trigger('translate');
		}

		if ($.support.transform3d && $.support.transition) {
			this.$stage.css({
				transform: 'translate3d(' + coordinate + 'px,0px,0px)',
				transition: (this.speed() / 1000) + 's'
			});
		} else if (animate) {
			this.$stage.animate({
				left: coordinate + 'px'
			}, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
		} else {
			this.$stage.css({
				left: coordinate + 'px'
			});
		}
	};

	/**
	 * Checks whether the carousel is in a specific state or not.
	 * @param {String} state - The state to check.
	 * @returns {Boolean} - The flag which indicates if the carousel is busy.
	 */
	Owl.prototype.is = function(state) {
		return this._states.current[state] && this._states.current[state] > 0;
	};

	/**
	 * Sets the absolute position of the current item.
	 * @public
	 * @param {Number} [position] - The new absolute position or nothing to leave it unchanged.
	 * @returns {Number} - The absolute position of the current item.
	 */
	Owl.prototype.current = function(position) {
		if (position === undefined) {
			return this._current;
		}

		if (this._items.length === 0) {
			return undefined;
		}

		position = this.normalize(position);

		if (this._current !== position) {
			var event = this.trigger('change', { property: { name: 'position', value: position } });

			if (event.data !== undefined) {
				position = this.normalize(event.data);
			}

			this._current = position;

			this.invalidate('position');

			this.trigger('changed', { property: { name: 'position', value: this._current } });
		}

		return this._current;
	};

	/**
	 * Invalidates the given part of the update routine.
	 * @param {String} [part] - The part to invalidate.
	 * @returns {Array.<String>} - The invalidated parts.
	 */
	Owl.prototype.invalidate = function(part) {
		if ($.type(part) === 'string') {
			this._invalidated[part] = true;
			this.is('valid') && this.leave('valid');
		}
		return $.map(this._invalidated, function(v, i) { return i });
	};

	/**
	 * Resets the absolute position of the current item.
	 * @public
	 * @param {Number} position - The absolute position of the new item.
	 */
	Owl.prototype.reset = function(position) {
		position = this.normalize(position);

		if (position === undefined) {
			return;
		}

		this._speed = 0;
		this._current = position;

		this.suppress([ 'translate', 'translated' ]);

		this.animate(this.coordinates(position));

		this.release([ 'translate', 'translated' ]);
	};

	/**
	 * Normalizes an absolute or a relative position of an item.
	 * @public
	 * @param {Number} position - The absolute or relative position to normalize.
	 * @param {Boolean} [relative=false] - Whether the given position is relative or not.
	 * @returns {Number} - The normalized position.
	 */
	Owl.prototype.normalize = function(position, relative) {
		var n = this._items.length,
			m = relative ? 0 : this._clones.length;

		if (!this.isNumeric(position) || n < 1) {
			position = undefined;
		} else if (position < 0 || position >= n + m) {
			position = ((position - m / 2) % n + n) % n + m / 2;
		}

		return position;
	};

	/**
	 * Converts an absolute position of an item into a relative one.
	 * @public
	 * @param {Number} position - The absolute position to convert.
	 * @returns {Number} - The converted position.
	 */
	Owl.prototype.relative = function(position) {
		position -= this._clones.length / 2;
		return this.normalize(position, true);
	};

	/**
	 * Gets the maximum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.maximum = function(relative) {
		var settings = this.settings,
			maximum = this._coordinates.length,
			iterator,
			reciprocalItemsWidth,
			elementWidth;

		if (settings.loop) {
			maximum = this._clones.length / 2 + this._items.length - 1;
		} else if (settings.autoWidth || settings.merge) {
			iterator = this._items.length;
			reciprocalItemsWidth = this._items[--iterator].width();
			elementWidth = this.$element.width();
			while (iterator--) {
				reciprocalItemsWidth += this._items[iterator].width() + this.settings.margin;
				if (reciprocalItemsWidth > elementWidth) {
					break;
				}
			}
			maximum = iterator + 1;
		} else if (settings.center) {
			maximum = this._items.length - 1;
		} else {
			maximum = this._items.length - settings.items;
		}

		if (relative) {
			maximum -= this._clones.length / 2;
		}

		return Math.max(maximum, 0);
	};

	/**
	 * Gets the minimum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.minimum = function(relative) {
		return relative ? 0 : this._clones.length / 2;
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.items = function(position) {
		if (position === undefined) {
			return this._items.slice();
		}

		position = this.normalize(position, true);
		return this._items[position];
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.mergers = function(position) {
		if (position === undefined) {
			return this._mergers.slice();
		}

		position = this.normalize(position, true);
		return this._mergers[position];
	};

	/**
	 * Gets the absolute positions of clones for an item.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @returns {Array.<Number>} - The absolute positions of clones for the item or all if no position was given.
	 */
	Owl.prototype.clones = function(position) {
		var odd = this._clones.length / 2,
			even = odd + this._items.length,
			map = function(index) { return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2 };

		if (position === undefined) {
			return $.map(this._clones, function(v, i) { return map(i) });
		}

		return $.map(this._clones, function(v, i) { return v === position ? map(i) : null });
	};

	/**
	 * Sets the current animation speed.
	 * @public
	 * @param {Number} [speed] - The animation speed in milliseconds or nothing to leave it unchanged.
	 * @returns {Number} - The current animation speed in milliseconds.
	 */
	Owl.prototype.speed = function(speed) {
		if (speed !== undefined) {
			this._speed = speed;
		}

		return this._speed;
	};

	/**
	 * Gets the coordinate of an item.
	 * @todo The name of this method is missleanding.
	 * @public
	 * @param {Number} position - The absolute position of the item within `minimum()` and `maximum()`.
	 * @returns {Number|Array.<Number>} - The coordinate of the item in pixel or all coordinates.
	 */
	Owl.prototype.coordinates = function(position) {
		var multiplier = 1,
			newPosition = position - 1,
			coordinate;

		if (position === undefined) {
			return $.map(this._coordinates, $.proxy(function(coordinate, index) {
				return this.coordinates(index);
			}, this));
		}

		if (this.settings.center) {
			if (this.settings.rtl) {
				multiplier = -1;
				newPosition = position + 1;
			}

			coordinate = this._coordinates[position];
			coordinate += (this.width() - coordinate + (this._coordinates[newPosition] || 0)) / 2 * multiplier;
		} else {
			coordinate = this._coordinates[newPosition] || 0;
		}

		coordinate = Math.ceil(coordinate);

		return coordinate;
	};

	/**
	 * Calculates the speed for a translation.
	 * @protected
	 * @param {Number} from - The absolute position of the start item.
	 * @param {Number} to - The absolute position of the target item.
	 * @param {Number} [factor=undefined] - The time factor in milliseconds.
	 * @returns {Number} - The time in milliseconds for the translation.
	 */
	Owl.prototype.duration = function(from, to, factor) {
		if (factor === 0) {
			return 0;
		}

		return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs((factor || this.settings.smartSpeed));
	};

	/**
	 * Slides to the specified item.
	 * @public
	 * @param {Number} position - The position of the item.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.to = function(position, speed) {
		var current = this.current(),
			revert = null,
			distance = position - this.relative(current),
			direction = (distance > 0) - (distance < 0),
			items = this._items.length,
			minimum = this.minimum(),
			maximum = this.maximum();

		if (this.settings.loop) {
			if (!this.settings.rewind && Math.abs(distance) > items / 2) {
				distance += direction * -1 * items;
			}

			position = current + distance;
			revert = ((position - minimum) % items + items) % items + minimum;

			if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
				current = revert - distance;
				position = revert;
				this.reset(current);
			}
		} else if (this.settings.rewind) {
			maximum += 1;
			position = (position % maximum + maximum) % maximum;
		} else {
			position = Math.max(minimum, Math.min(maximum, position));
		}

		this.speed(this.duration(current, position, speed));
		this.current(position);

		if (this.$element.is(':visible')) {
			this.update();
		}
	};

	/**
	 * Slides to the next item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.next = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) + 1, speed);
	};

	/**
	 * Slides to the previous item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.prev = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) - 1, speed);
	};

	/**
	 * Handles the end of an animation.
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onTransitionEnd = function(event) {

		// if css2 animation then event object is undefined
		if (event !== undefined) {
			event.stopPropagation();

			// Catch only owl-stage transitionEnd event
			if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
				return false;
			}
		}

		this.leave('animating');
		this.trigger('translated');
	};

	/**
	 * Gets viewport width.
	 * @protected
	 * @return {Number} - The width in pixel.
	 */
	Owl.prototype.viewport = function() {
		var width;
		if (this.options.responsiveBaseElement !== window) {
			width = $(this.options.responsiveBaseElement).width();
		} else if (window.innerWidth) {
			width = window.innerWidth;
		} else if (document.documentElement && document.documentElement.clientWidth) {
			width = document.documentElement.clientWidth;
		} else {
			console.warn('Can not detect viewport width.');
		}
		return width;
	};

	/**
	 * Replaces the current content.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The new content.
	 */
	Owl.prototype.replace = function(content) {
		this.$stage.empty();
		this._items = [];

		if (content) {
			content = (content instanceof jQuery) ? content : $(content);
		}

		if (this.settings.nestedItemSelector) {
			content = content.find('.' + this.settings.nestedItemSelector);
		}

		content.filter(function() {
			return this.nodeType === 1;
		}).each($.proxy(function(index, item) {
			item = this.prepare(item);
			this.$stage.append(item);
			this._items.push(item);
			this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}, this));

		this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);

		this.invalidate('items');
	};

	/**
	 * Adds an item.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The item content to add.
	 * @param {Number} [position] - The relative position at which to insert the item otherwise the item will be added to the end.
	 */
	Owl.prototype.add = function(content, position) {
		var current = this.relative(this._current);

		position = position === undefined ? this._items.length : this.normalize(position, true);
		content = content instanceof jQuery ? content : $(content);

		this.trigger('add', { content: content, position: position });

		content = this.prepare(content);

		if (this._items.length === 0 || position === this._items.length) {
			this._items.length === 0 && this.$stage.append(content);
			this._items.length !== 0 && this._items[position - 1].after(content);
			this._items.push(content);
			this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		} else {
			this._items[position].before(content);
			this._items.splice(position, 0, content);
			this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}

		this._items[current] && this.reset(this._items[current].index());

		this.invalidate('items');

		this.trigger('added', { content: content, position: position });
	};

	/**
	 * Removes an item by its position.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {Number} position - The relative position of the item to remove.
	 */
	Owl.prototype.remove = function(position) {
		position = this.normalize(position, true);

		if (position === undefined) {
			return;
		}

		this.trigger('remove', { content: this._items[position], position: position });

		this._items[position].remove();
		this._items.splice(position, 1);
		this._mergers.splice(position, 1);

		this.invalidate('items');

		this.trigger('removed', { content: null, position: position });
	};

	/**
	 * Preloads images with auto width.
	 * @todo Replace by a more generic approach
	 * @protected
	 */
	Owl.prototype.preloadAutoWidthImages = function(images) {
		images.each($.proxy(function(i, element) {
			this.enter('pre-loading');
			element = $(element);
			$(new Image()).one('load', $.proxy(function(e) {
				element.attr('src', e.target.src);
				element.css('opacity', 1);
				this.leave('pre-loading');
				!this.is('pre-loading') && !this.is('initializing') && this.refresh();
			}, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
		}, this));
	};

	/**
	 * Destroys the carousel.
	 * @public
	 */
	Owl.prototype.destroy = function() {

		this.$element.off('.owl.core');
		this.$stage.off('.owl.core');
		$(document).off('.owl.core');

		if (this.settings.responsive !== false) {
			window.clearTimeout(this.resizeTimer);
			this.off(window, 'resize', this._handlers.onThrottledResize);
		}

		for (var i in this._plugins) {
			this._plugins[i].destroy();
		}

		this.$stage.children('.cloned').remove();

		this.$stage.unwrap();
		this.$stage.children().contents().unwrap();
		this.$stage.children().unwrap();

		this.$element
			.removeClass(this.options.refreshClass)
			.removeClass(this.options.loadingClass)
			.removeClass(this.options.loadedClass)
			.removeClass(this.options.rtlClass)
			.removeClass(this.options.dragClass)
			.removeClass(this.options.grabClass)
			.attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), ''))
			.removeData('owl.carousel');
	};

	/**
	 * Operators to calculate right-to-left and left-to-right.
	 * @protected
	 * @param {Number} [a] - The left side operand.
	 * @param {String} [o] - The operator.
	 * @param {Number} [b] - The right side operand.
	 */
	Owl.prototype.op = function(a, o, b) {
		var rtl = this.settings.rtl;
		switch (o) {
			case '<':
				return rtl ? a > b : a < b;
			case '>':
				return rtl ? a < b : a > b;
			case '>=':
				return rtl ? a <= b : a >= b;
			case '<=':
				return rtl ? a >= b : a <= b;
			default:
				break;
		}
	};

	/**
	 * Attaches to an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The event handler to attach.
	 * @param {Boolean} capture - Wether the event should be handled at the capturing phase or not.
	 */
	Owl.prototype.on = function(element, event, listener, capture) {
		if (element.addEventListener) {
			element.addEventListener(event, listener, capture);
		} else if (element.attachEvent) {
			element.attachEvent('on' + event, listener);
		}
	};

	/**
	 * Detaches from an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The attached event handler to detach.
	 * @param {Boolean} capture - Wether the attached event handler was registered as a capturing listener or not.
	 */
	Owl.prototype.off = function(element, event, listener, capture) {
		if (element.removeEventListener) {
			element.removeEventListener(event, listener, capture);
		} else if (element.detachEvent) {
			element.detachEvent('on' + event, listener);
		}
	};

	/**
	 * Triggers a public event.
	 * @todo Remove `status`, `relatedTarget` should be used instead.
	 * @protected
	 * @param {String} name - The event name.
	 * @param {*} [data=null] - The event data.
	 * @param {String} [namespace=carousel] - The event namespace.
	 * @param {String} [state] - The state which is associated with the event.
	 * @param {Boolean} [enter=false] - Indicates if the call enters the specified state or not.
	 * @returns {Event} - The event arguments.
	 */
	Owl.prototype.trigger = function(name, data, namespace, state, enter) {
		var status = {
			item: { count: this._items.length, index: this.current() }
		}, handler = $.camelCase(
			$.grep([ 'on', name, namespace ], function(v) { return v })
				.join('-').toLowerCase()
		), event = $.Event(
			[ name, 'owl', namespace || 'carousel' ].join('.').toLowerCase(),
			$.extend({ relatedTarget: this }, status, data)
		);

		if (!this._supress[name]) {
			$.each(this._plugins, function(name, plugin) {
				if (plugin.onTrigger) {
					plugin.onTrigger(event);
				}
			});

			this.register({ type: Owl.Type.Event, name: name });
			this.$element.trigger(event);

			if (this.settings && typeof this.settings[handler] === 'function') {
				this.settings[handler].call(this, event);
			}
		}

		return event;
	};

	/**
	 * Enters a state.
	 * @param name - The state name.
	 */
	Owl.prototype.enter = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			if (this._states.current[name] === undefined) {
				this._states.current[name] = 0;
			}

			this._states.current[name]++;
		}, this));
	};

	/**
	 * Leaves a state.
	 * @param name - The state name.
	 */
	Owl.prototype.leave = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			this._states.current[name]--;
		}, this));
	};

	/**
	 * Registers an event or state.
	 * @public
	 * @param {Object} object - The event or state to register.
	 */
	Owl.prototype.register = function(object) {
		if (object.type === Owl.Type.Event) {
			if (!$.event.special[object.name]) {
				$.event.special[object.name] = {};
			}

			if (!$.event.special[object.name].owl) {
				var _default = $.event.special[object.name]._default;
				$.event.special[object.name]._default = function(e) {
					if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
						return _default.apply(this, arguments);
					}
					return e.namespace && e.namespace.indexOf('owl') > -1;
				};
				$.event.special[object.name].owl = true;
			}
		} else if (object.type === Owl.Type.State) {
			if (!this._states.tags[object.name]) {
				this._states.tags[object.name] = object.tags;
			} else {
				this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
			}

			this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function(tag, i) {
				return $.inArray(tag, this._states.tags[object.name]) === i;
			}, this));
		}
	};

	/**
	 * Suppresses events.
	 * @protected
	 * @param {Array.<String>} events - The events to suppress.
	 */
	Owl.prototype.suppress = function(events) {
		$.each(events, $.proxy(function(index, event) {
			this._supress[event] = true;
		}, this));
	};

	/**
	 * Releases suppressed events.
	 * @protected
	 * @param {Array.<String>} events - The events to release.
	 */
	Owl.prototype.release = function(events) {
		$.each(events, $.proxy(function(index, event) {
			delete this._supress[event];
		}, this));
	};

	/**
	 * Gets unified pointer coordinates from event.
	 * @todo #261
	 * @protected
	 * @param {Event} - The `mousedown` or `touchstart` event.
	 * @returns {Object} - Contains `x` and `y` coordinates of current pointer position.
	 */
	Owl.prototype.pointer = function(event) {
		var result = { x: null, y: null };

		event = event.originalEvent || event || window.event;

		event = event.touches && event.touches.length ?
			event.touches[0] : event.changedTouches && event.changedTouches.length ?
				event.changedTouches[0] : event;

		if (event.pageX) {
			result.x = event.pageX;
			result.y = event.pageY;
		} else {
			result.x = event.clientX;
			result.y = event.clientY;
		}

		return result;
	};

	/**
	 * Determines if the input is a Number or something that can be coerced to a Number
	 * @protected
	 * @param {Number|String|Object|Array|Boolean|RegExp|Function|Symbol} - The input to be tested
	 * @returns {Boolean} - An indication if the input is a Number or can be coerced to a Number
	 */
	Owl.prototype.isNumeric = function(number) {
		return !isNaN(parseFloat(number));
	};

	/**
	 * Gets the difference of two vectors.
	 * @todo #261
	 * @protected
	 * @param {Object} - The first vector.
	 * @param {Object} - The second vector.
	 * @returns {Object} - The difference.
	 */
	Owl.prototype.difference = function(first, second) {
		return {
			x: first.x - second.x,
			y: first.y - second.y
		};
	};

	/**
	 * The jQuery Plugin for the Owl Carousel
	 * @todo Navigation plugin `next` and `prev`
	 * @public
	 */
	$.fn.owlCarousel = function(option) {
		var args = Array.prototype.slice.call(arguments, 1);

		return this.each(function() {
			var $this = $(this),
				data = $this.data('owl.carousel');

			if (!data) {
				data = new Owl(this, typeof option == 'object' && option);
				$this.data('owl.carousel', data);

				$.each([
					'next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'
				], function(i, event) {
					data.register({ type: Owl.Type.Event, name: event });
					data.$element.on(event + '.owl.carousel.core', $.proxy(function(e) {
						if (e.namespace && e.relatedTarget !== this) {
							this.suppress([ event ]);
							data[event].apply(this, [].slice.call(arguments, 1));
							this.release([ event ]);
						}
					}, data));
				});
			}

			if (typeof option == 'string' && option.charAt(0) !== '_') {
				data[option].apply(data, args);
			}
		});
	};

	/**
	 * The constructor for the jQuery Plugin
	 * @public
	 */
	$.fn.owlCarousel.Constructor = Owl;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoRefresh Plugin
 * @version 2.1.0
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto refresh plugin.
	 * @class The Auto Refresh Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoRefresh = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Refresh interval.
		 * @protected
		 * @type {number}
		 */
		this._interval = null;

		/**
		 * Whether the element is currently visible or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._visible = null;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoRefresh) {
					this.watch();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	AutoRefresh.Defaults = {
		autoRefresh: true,
		autoRefreshInterval: 500
	};

	/**
	 * Watches the element.
	 */
	AutoRefresh.prototype.watch = function() {
		if (this._interval) {
			return;
		}

		this._visible = this._core.$element.is(':visible');
		this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
	};

	/**
	 * Refreshes the element.
	 */
	AutoRefresh.prototype.refresh = function() {
		if (this._core.$element.is(':visible') === this._visible) {
			return;
		}

		this._visible = !this._visible;

		this._core.$element.toggleClass('owl-hidden', !this._visible);

		this._visible && (this._core.invalidate('width') && this._core.refresh());
	};

	/**
	 * Destroys the plugin.
	 */
	AutoRefresh.prototype.destroy = function() {
		var handler, property;

		window.clearInterval(this._interval);

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;

})(window.Zepto || window.jQuery, window, document);

/**
 * Lazy Plugin
 * @version 2.1.0
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the lazy plugin.
	 * @class The Lazy Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Lazy = function(carousel) {

		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Already loaded items.
		 * @protected
		 * @type {Array.<jQuery>}
		 */
		this._loaded = [];

		/**
		 * Event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				if (!this._core.settings || !this._core.settings.lazyLoad) {
					return;
				}

				if ((e.property && e.property.name == 'position') || e.type == 'initialized') {
					var settings = this._core.settings,
						n = (settings.center && Math.ceil(settings.items / 2) || settings.items),
						i = ((settings.center && n * -1) || 0),
						position = (e.property && e.property.value !== undefined ? e.property.value : this._core.current()) + i,
						clones = this._core.clones().length,
						load = $.proxy(function(i, v) { this.load(v) }, this);

					while (i++ < n) {
						this.load(clones / 2 + this._core.relative(position));
						clones && $.each(this._core.clones(this._core.relative(position)), load);
						position++;
					}
				}
			}, this)
		};

		// set the default options
		this._core.options = $.extend({}, Lazy.Defaults, this._core.options);

		// register event handler
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	Lazy.Defaults = {
		lazyLoad: false
	};

	/**
	 * Loads all resources of an item at the specified position.
	 * @param {Number} position - The absolute position of the item.
	 * @protected
	 */
	Lazy.prototype.load = function(position) {
		var $item = this._core.$stage.children().eq(position),
			$elements = $item && $item.find('.owl-lazy');

		if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
			return;
		}

		$elements.each($.proxy(function(index, element) {
			var $element = $(element), image,
				url = (window.devicePixelRatio > 1 && $element.attr('data-src-retina')) || $element.attr('data-src');

			this._core.trigger('load', { element: $element, url: url }, 'lazy');

			if ($element.is('img')) {
				$element.one('load.owl.lazy', $.proxy(function() {
					$element.css('opacity', 1);
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this)).attr('src', url);
			} else {
				image = new Image();
				image.onload = $.proxy(function() {
					$element.css({
						'background-image': 'url("' + url + '")',
						'opacity': '1'
					});
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this);
				image.src = url;
			}
		}, this));

		this._loaded.push($item.get(0));
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Lazy.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this._core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoHeight Plugin
 * @version 2.1.0
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto height plugin.
	 * @class The Auto Height Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoHeight = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight) {
					this.update();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight && e.property.name == 'position'){
					this.update();
				}
			}, this),
			'loaded.owl.lazy': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight
					&& e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
					this.update();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	AutoHeight.Defaults = {
		autoHeight: false,
		autoHeightClass: 'owl-height'
	};

	/**
	 * Updates the view.
	 */
	AutoHeight.prototype.update = function() {
		var start = this._core._current,
			end = start + this._core.settings.items,
			visible = this._core.$stage.children().toArray().slice(start, end),
			heights = [],
			maxheight = 0;

		$.each(visible, function(index, item) {
			heights.push($(item).height());
		});

		maxheight = Math.max.apply(null, heights);

		this._core.$stage.parent()
			.height(maxheight)
			.addClass(this._core.settings.autoHeightClass);
	};

	AutoHeight.prototype.destroy = function() {
		var handler, property;

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;

})(window.Zepto || window.jQuery, window, document);

/**
 * Video Plugin
 * @version 2.1.0
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the video plugin.
	 * @class The Video Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Video = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Cache all video URLs.
		 * @protected
		 * @type {Object}
		 */
		this._videos = {};

		/**
		 * Current playing item.
		 * @protected
		 * @type {jQuery}
		 */
		this._playing = null;

		/**
		 * All event handlers.
		 * @todo The cloned content removale is too late
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this._core.register({ type: 'state', name: 'playing', tags: [ 'interacting' ] });
				}
			}, this),
			'resize.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
					e.preventDefault();
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.is('resizing')) {
					this._core.$stage.find('.cloned .owl-video-frame').remove();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position' && this._playing) {
					this.stop();
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				var $element = $(e.content).find('.owl-video');

				if ($element.length) {
					$element.css('display', 'none');
					this.fetch($element, $(e.content));
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Video.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);

		this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function(e) {
			this.play(e);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Video.Defaults = {
		video: false,
		videoHeight: false,
		videoWidth: false
	};

	/**
	 * Gets the video ID and the type (YouTube/Vimeo/vzaar only).
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {jQuery} item - The item containing the video.
	 */
	Video.prototype.fetch = function(target, item) {
			var type = (function() {
					if (target.attr('data-vimeo-id')) {
						return 'vimeo';
					} else if (target.attr('data-vzaar-id')) {
						return 'vzaar'
					} else {
						return 'youtube';
					}
				})(),
				id = target.attr('data-vimeo-id') || target.attr('data-youtube-id') || target.attr('data-vzaar-id'),
				width = target.attr('data-width') || this._core.settings.videoWidth,
				height = target.attr('data-height') || this._core.settings.videoHeight,
				url = target.attr('href');

		if (url) {

			/*
					Parses the id's out of the following urls (and probably more):
					https://www.youtube.com/watch?v=:id
					https://youtu.be/:id
					https://vimeo.com/:id
					https://vimeo.com/channels/:channel/:id
					https://vimeo.com/groups/:group/videos/:id
					https://app.vzaar.com/videos/:id

					Visual example: https://regexper.com/#(http%3A%7Chttps%3A%7C)%5C%2F%5C%2F(player.%7Cwww.%7Capp.)%3F(vimeo%5C.com%7Cyoutu(be%5C.com%7C%5C.be%7Cbe%5C.googleapis%5C.com)%7Cvzaar%5C.com)%5C%2F(video%5C%2F%7Cvideos%5C%2F%7Cembed%5C%2F%7Cchannels%5C%2F.%2B%5C%2F%7Cgroups%5C%2F.%2B%5C%2F%7Cwatch%5C%3Fv%3D%7Cv%5C%2F)%3F(%5BA-Za-z0-9._%25-%5D*)(%5C%26%5CS%2B)%3F
			*/

			id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

			if (id[3].indexOf('youtu') > -1) {
				type = 'youtube';
			} else if (id[3].indexOf('vimeo') > -1) {
				type = 'vimeo';
			} else if (id[3].indexOf('vzaar') > -1) {
				type = 'vzaar';
			} else {
				throw new Error('Video URL not supported.');
			}
			id = id[6];
		} else {
			throw new Error('Missing video URL.');
		}

		this._videos[url] = {
			type: type,
			id: id,
			width: width,
			height: height
		};

		item.attr('data-video', url);

		this.thumbnail(target, this._videos[url]);
	};

	/**
	 * Creates video thumbnail.
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {Object} info - The video info object.
	 * @see `fetch`
	 */
	Video.prototype.thumbnail = function(target, video) {
		var tnLink,
			icon,
			path,
			dimensions = video.width && video.height ? 'style="width:' + video.width + 'px;height:' + video.height + 'px;"' : '',
			customTn = target.find('img'),
			srcType = 'src',
			lazyClass = '',
			settings = this._core.settings,
			create = function(path) {
				icon = '<div class="owl-video-play-icon"></div>';

				if (settings.lazyLoad) {
					tnLink = '<div class="owl-video-tn ' + lazyClass + '" ' + srcType + '="' + path + '"></div>';
				} else {
					tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>';
				}
				target.after(tnLink);
				target.after(icon);
			};

		// wrap video content into owl-video-wrapper div
		target.wrap('<div class="owl-video-wrapper"' + dimensions + '></div>');

		if (this._core.settings.lazyLoad) {
			srcType = 'data-src';
			lazyClass = 'owl-lazy';
		}

		// custom thumbnail
		if (customTn.length) {
			create(customTn.attr(srcType));
			customTn.remove();
			return false;
		}

		if (video.type === 'youtube') {
			path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
			create(path);
		} else if (video.type === 'vimeo') {
			$.ajax({
				type: 'GET',
				url: '//vimeo.com/api/v2/video/' + video.id + '.json',
				jsonp: 'callback',
				dataType: 'jsonp',
				success: function(data) {
					path = data[0].thumbnail_large;
					create(path);
				}
			});
		} else if (video.type === 'vzaar') {
			$.ajax({
				type: 'GET',
				url: '//vzaar.com/api/videos/' + video.id + '.json',
				jsonp: 'callback',
				dataType: 'jsonp',
				success: function(data) {
					path = data.framegrab_url;
					create(path);
				}
			});
		}
	};

	/**
	 * Stops the current video.
	 * @public
	 */
	Video.prototype.stop = function() {
		this._core.trigger('stop', null, 'video');
		this._playing.find('.owl-video-frame').remove();
		this._playing.removeClass('owl-video-playing');
		this._playing = null;
		this._core.leave('playing');
		this._core.trigger('stopped', null, 'video');
	};

	/**
	 * Starts the current video.
	 * @public
	 * @param {Event} event - The event arguments.
	 */
	Video.prototype.play = function(event) {
		var target = $(event.target),
			item = target.closest('.' + this._core.settings.itemClass),
			video = this._videos[item.attr('data-video')],
			width = video.width || '100%',
			height = video.height || this._core.$stage.height(),
			html;

		if (this._playing) {
			return;
		}

		this._core.enter('playing');
		this._core.trigger('play', null, 'video');

		item = this._core.items(this._core.relative(item.index()));

		this._core.reset(item.index());

		if (video.type === 'youtube') {
			html = '<iframe width="' + width + '" height="' + height + '" src="//www.youtube.com/embed/' +
				video.id + '?autoplay=1&rel=0&v=' + video.id + '" frameborder="0" allowfullscreen></iframe>';
		} else if (video.type === 'vimeo') {
			html = '<iframe src="//player.vimeo.com/video/' + video.id +
				'?autoplay=1" width="' + width + '" height="' + height +
				'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		} else if (video.type === 'vzaar') {
			html = '<iframe frameborder="0"' + 'height="' + height + '"' + 'width="' + width +
				'" allowfullscreen mozallowfullscreen webkitAllowFullScreen ' +
				'src="//view.vzaar.com/' + video.id + '/player?autoplay=true"></iframe>';
		}

		$('<div class="owl-video-frame">' + html + '</div>').insertAfter(item.find('.owl-video'));

		this._playing = item.addClass('owl-video-playing');
	};

	/**
	 * Checks whether an video is currently in full screen mode or not.
	 * @todo Bad style because looks like a readonly method but changes members.
	 * @protected
	 * @returns {Boolean}
	 */
	Video.prototype.isInFullScreen = function() {
		var element = document.fullscreenElement || document.mozFullScreenElement ||
				document.webkitFullscreenElement;

		return element && $(element).parent().hasClass('owl-video-frame');
	};

	/**
	 * Destroys the plugin.
	 */
	Video.prototype.destroy = function() {
		var handler, property;

		this._core.$element.off('click.owl.video');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Video = Video;

})(window.Zepto || window.jQuery, window, document);

/**
 * Animate Plugin
 * @version 2.1.0
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the animate plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Animate = function(scope) {
		this.core = scope;
		this.core.options = $.extend({}, Animate.Defaults, this.core.options);
		this.swapping = true;
		this.previous = undefined;
		this.next = undefined;

		this.handlers = {
			'change.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.previous = this.core.current();
					this.next = e.property.value;
				}
			}, this),
			'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this.swapping = e.type == 'translated';
				}
			}, this),
			'translate.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
					this.swap();
				}
			}, this)
		};

		this.core.$element.on(this.handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	Animate.Defaults = {
		animateOut: false,
		animateIn: false
	};

	/**
	 * Toggles the animation classes whenever an translations starts.
	 * @protected
	 * @returns {Boolean|undefined}
	 */
	Animate.prototype.swap = function() {

		if (this.core.settings.items !== 1) {
			return;
		}

		if (!$.support.animation || !$.support.transition) {
			return;
		}

		this.core.speed(0);

		var left,
			clear = $.proxy(this.clear, this),
			previous = this.core.$stage.children().eq(this.previous),
			next = this.core.$stage.children().eq(this.next),
			incoming = this.core.settings.animateIn,
			outgoing = this.core.settings.animateOut;

		if (this.core.current() === this.previous) {
			return;
		}

		if (outgoing) {
			left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
			previous.one($.support.animation.end, clear)
				.css( { 'left': left + 'px' } )
				.addClass('animated owl-animated-out')
				.addClass(outgoing);
		}

		if (incoming) {
			next.one($.support.animation.end, clear)
				.addClass('animated owl-animated-in')
				.addClass(incoming);
		}
	};

	Animate.prototype.clear = function(e) {
		$(e.target).css( { 'left': '' } )
			.removeClass('animated owl-animated-out owl-animated-in')
			.removeClass(this.core.settings.animateIn)
			.removeClass(this.core.settings.animateOut);
		this.core.onTransitionEnd();
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Animate.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this.core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Animate = Animate;

})(window.Zepto || window.jQuery, window, document);

/**
 * Autoplay Plugin
 * @version 2.1.0
 * @author Bartosz Wojciechowski
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the autoplay plugin.
	 * @class The Autoplay Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Autoplay = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * The autoplay timeout.
		 * @type {Timeout}
		 */
		this._timeout = null;

		/**
		 * Indicates whenever the autoplay is paused.
		 * @type {Boolean}
		 */
		this._paused = false;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'settings') {
					if (this._core.settings.autoplay) {
						this.play();
					} else {
						this.stop();
					}
				} else if (e.namespace && e.property.name === 'position') {
					//console.log('play?', e);
					if (this._core.settings.autoplay) {
						this._setAutoPlayInterval();
					}
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoplay) {
					this.play();
				}
			}, this),
			'play.owl.autoplay': $.proxy(function(e, t, s) {
				if (e.namespace) {
					this.play(t, s);
				}
			}, this),
			'stop.owl.autoplay': $.proxy(function(e) {
				if (e.namespace) {
					this.stop();
				}
			}, this),
			'mouseover.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.pause();
				}
			}, this),
			'mouseleave.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.play();
				}
			}, this),
			'touchstart.owl.core': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.pause();
				}
			}, this),
			'touchend.owl.core': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause) {
					this.play();
				}
			}, this)
		};

		// register event handlers
		this._core.$element.on(this._handlers);

		// set default options
		this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
	};

	/**
	 * Default options.
	 * @public
	 */
	Autoplay.Defaults = {
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoplaySpeed: false
	};

	/**
	 * Starts the autoplay.
	 * @public
	 * @param {Number} [timeout] - The interval before the next animation starts.
	 * @param {Number} [speed] - The animation speed for the animations.
	 */
	Autoplay.prototype.play = function(timeout, speed) {
		this._paused = false;

		if (this._core.is('rotating')) {
			return;
		}

		this._core.enter('rotating');

		this._setAutoPlayInterval();
	};

	/**
	 * Gets a new timeout
	 * @private
	 * @param {Number} [timeout] - The interval before the next animation starts.
	 * @param {Number} [speed] - The animation speed for the animations.
	 * @return {Timeout}
	 */
	Autoplay.prototype._getNextTimeout = function(timeout, speed) {
		if ( this._timeout ) {
			window.clearTimeout(this._timeout);
		}
		return window.setTimeout($.proxy(function() {
			if (this._paused || this._core.is('busy') || this._core.is('interacting') || document.hidden) {
				return;
			}
			this._core.next(speed || this._core.settings.autoplaySpeed);
		}, this), timeout || this._core.settings.autoplayTimeout);
	};

	/**
	 * Sets autoplay in motion.
	 * @private
	 */
	Autoplay.prototype._setAutoPlayInterval = function() {
		this._timeout = this._getNextTimeout();
	};

	/**
	 * Stops the autoplay.
	 * @public
	 */
	Autoplay.prototype.stop = function() {
		if (!this._core.is('rotating')) {
			return;
		}

		window.clearTimeout(this._timeout);
		this._core.leave('rotating');
	};

	/**
	 * Stops the autoplay.
	 * @public
	 */
	Autoplay.prototype.pause = function() {
		if (!this._core.is('rotating')) {
			return;
		}

		this._paused = true;
	};

	/**
	 * Destroys the plugin.
	 */
	Autoplay.prototype.destroy = function() {
		var handler, property;

		this.stop();

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;

})(window.Zepto || window.jQuery, window, document);

/**
 * Navigation Plugin
 * @version 2.1.0
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the navigation plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} carousel - The Owl Carousel.
	 */
	var Navigation = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Indicates whether the plugin is initialized or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._initialized = false;

		/**
		 * The current paging indexes.
		 * @protected
		 * @type {Array}
		 */
		this._pages = [];

		/**
		 * All DOM elements of the user interface.
		 * @protected
		 * @type {Object}
		 */
		this._controls = {};

		/**
		 * Markup for an indicator.
		 * @protected
		 * @type {Array.<String>}
		 */
		this._templates = [];

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * Overridden methods of the carousel.
		 * @protected
		 * @type {Object}
		 */
		this._overrides = {
			next: this._core.next,
			prev: this._core.prev,
			to: this._core.to
		};

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.push('<div class="' + this._core.settings.dotClass + '">' +
						$(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
				}
			}, this),
			'added.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 0, this._templates.pop());
				}
			}, this),
			'remove.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 1);
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.draw();
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && !this._initialized) {
					this._core.trigger('initialize', null, 'navigation');
					this.initialize();
					this.update();
					this.draw();
					this._initialized = true;
					this._core.trigger('initialized', null, 'navigation');
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._initialized) {
					this._core.trigger('refresh', null, 'navigation');
					this.update();
					this.draw();
					this._core.trigger('refreshed', null, 'navigation');
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Navigation.Defaults, this._core.options);

		// register event handlers
		this.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 * @todo Rename `slideBy` to `navBy`
	 */
	Navigation.Defaults = {
		nav: false,
		navText: [ 'prev', 'next' ],
		navSpeed: false,
		navElement: 'div',
		navContainer: false,
		navContainerClass: 'owl-nav',
		navClass: [ 'owl-prev', 'owl-next' ],
		slideBy: 1,
		dotClass: 'owl-dot',
		dotsClass: 'owl-dots',
		dots: true,
		dotsEach: false,
		dotsData: false,
		dotsSpeed: false,
		dotsContainer: false
	};

	/**
	 * Initializes the layout of the plugin and extends the carousel.
	 * @protected
	 */
	Navigation.prototype.initialize = function() {
		var override,
			settings = this._core.settings;

		// create DOM structure for relative navigation
		this._controls.$relative = (settings.navContainer ? $(settings.navContainer)
			: $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$previous = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[0])
			.html(settings.navText[0])
			.prependTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.prev(settings.navSpeed);
			}, this));
		this._controls.$next = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[1])
			.html(settings.navText[1])
			.appendTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.next(settings.navSpeed);
			}, this));

		// create DOM structure for absolute navigation
		if (!settings.dotsData) {
			this._templates = [ $('<div>')
				.addClass(settings.dotClass)
				.append($('<span>'))
				.prop('outerHTML') ];
		}

		this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer)
			: $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$absolute.on('click', 'div', $.proxy(function(e) {
			var index = $(e.target).parent().is(this._controls.$absolute)
				? $(e.target).index() : $(e.target).parent().index();

			e.preventDefault();

			this.to(index, settings.dotsSpeed);
		}, this));

		// override public methods of the carousel
		for (override in this._overrides) {
			this._core[override] = $.proxy(this[override], this);
		}
	};

	/**
	 * Destroys the plugin.
	 * @protected
	 */
	Navigation.prototype.destroy = function() {
		var handler, control, property, override;

		for (handler in this._handlers) {
			this.$element.off(handler, this._handlers[handler]);
		}
		for (control in this._controls) {
			this._controls[control].remove();
		}
		for (override in this.overides) {
			this._core[override] = this._overrides[override];
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	/**
	 * Updates the internal state.
	 * @protected
	 */
	Navigation.prototype.update = function() {
		var i, j, k,
			lower = this._core.clones().length / 2,
			upper = lower + this._core.items().length,
			maximum = this._core.maximum(true),
			settings = this._core.settings,
			size = settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items;

		if (settings.slideBy !== 'page') {
			settings.slideBy = Math.min(settings.slideBy, settings.items);
		}

		if (settings.dots || settings.slideBy == 'page') {
			this._pages = [];

			for (i = lower, j = 0, k = 0; i < upper; i++) {
				if (j >= size || j === 0) {
					this._pages.push({
						start: Math.min(maximum, i - lower),
						end: i - lower + size - 1
					});
					if (Math.min(maximum, i - lower) === maximum) {
						break;
					}
					j = 0, ++k;
				}
				j += this._core.mergers(this._core.relative(i));
			}
		}
	};

	/**
	 * Draws the user interface.
	 * @todo The option `dotsData` wont work.
	 * @protected
	 */
	Navigation.prototype.draw = function() {
		var difference,
			settings = this._core.settings,
			disabled = this._core.items().length <= settings.items,
			index = this._core.relative(this._core.current()),
			loop = settings.loop || settings.rewind;

		this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);

		if (settings.nav) {
			this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));
			this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
		}

		this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);

		if (settings.dots) {
			difference = this._pages.length - this._controls.$absolute.children().length;

			if (settings.dotsData && difference !== 0) {
				this._controls.$absolute.html(this._templates.join(''));
			} else if (difference > 0) {
				this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
			} else if (difference < 0) {
				this._controls.$absolute.children().slice(difference).remove();
			}

			this._controls.$absolute.find('.active').removeClass('active');
			this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
		}
	};

	/**
	 * Extends event data.
	 * @protected
	 * @param {Event} event - The event object which gets thrown.
	 */
	Navigation.prototype.onTrigger = function(event) {
		var settings = this._core.settings;

		event.page = {
			index: $.inArray(this.current(), this._pages),
			count: this._pages.length,
			size: settings && (settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items)
		};
	};

	/**
	 * Gets the current page position of the carousel.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.current = function() {
		var current = this._core.relative(this._core.current());
		return $.grep(this._pages, $.proxy(function(page, index) {
			return page.start <= current && page.end >= current;
		}, this)).pop();
	};

	/**
	 * Gets the current succesor/predecessor position.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.getPosition = function(successor) {
		var position, length,
			settings = this._core.settings;

		if (settings.slideBy == 'page') {
			position = $.inArray(this.current(), this._pages);
			length = this._pages.length;
			successor ? ++position : --position;
			position = this._pages[((position % length) + length) % length].start;
		} else {
			position = this._core.relative(this._core.current());
			length = this._core.items().length;
			successor ? position += settings.slideBy : position -= settings.slideBy;
		}

		return position;
	};

	/**
	 * Slides to the next item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.next = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
	};

	/**
	 * Slides to the previous item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.prev = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
	};

	/**
	 * Slides to the specified item or page.
	 * @public
	 * @param {Number} position - The position of the item or page.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 * @param {Boolean} [standard=false] - Whether to use the standard behaviour or not.
	 */
	Navigation.prototype.to = function(position, speed, standard) {
		var length;

		if (!standard && this._pages.length) {
			length = this._pages.length;
			$.proxy(this._overrides.to, this._core)(this._pages[((position % length) + length) % length].start, speed);
		} else {
			$.proxy(this._overrides.to, this._core)(position, speed);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;

})(window.Zepto || window.jQuery, window, document);

/**
 * Hash Plugin
 * @version 2.1.0
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the hash plugin.
	 * @class The Hash Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Hash = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Hash index for the items.
		 * @protected
		 * @type {Object}
		 */
		this._hashes = {};

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.startPosition === 'URLHash') {
					$(window).trigger('hashchange.owl.navigation');
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');

					if (!hash) {
						return;
					}

					this._hashes[hash] = e.content;
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position') {
					var current = this._core.items(this._core.relative(this._core.current())),
						hash = $.map(this._hashes, function(item, hash) {
							return item === current ? hash : null;
						}).join();

					if (!hash || window.location.hash.slice(1) === hash) {
						return;
					}

					window.location.hash = hash;
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Hash.Defaults, this._core.options);

		// register the event handlers
		this.$element.on(this._handlers);

		// register event listener for hash navigation
		$(window).on('hashchange.owl.navigation', $.proxy(function(e) {
			var hash = window.location.hash.substring(1),
				items = this._core.$stage.children(),
				position = this._hashes[hash] && items.index(this._hashes[hash]);

			if (position === undefined || position === this._core.current()) {
				return;
			}

			this._core.to(this._core.relative(position), false, true);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Hash.Defaults = {
		URLhashListener: false
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Hash.prototype.destroy = function() {
		var handler, property;

		$(window).off('hashchange.owl.navigation');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Hash = Hash;

})(window.Zepto || window.jQuery, window, document);

/**
 * Support Plugin
 *
 * @version 2.1.0
 * @author Vivid Planet Software GmbH
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	var style = $('<support>').get(0).style,
		prefixes = 'Webkit Moz O ms'.split(' '),
		events = {
			transition: {
				end: {
					WebkitTransition: 'webkitTransitionEnd',
					MozTransition: 'transitionend',
					OTransition: 'oTransitionEnd',
					transition: 'transitionend'
				}
			},
			animation: {
				end: {
					WebkitAnimation: 'webkitAnimationEnd',
					MozAnimation: 'animationend',
					OAnimation: 'oAnimationEnd',
					animation: 'animationend'
				}
			}
		},
		tests = {
			csstransforms: function() {
				return !!test('transform');
			},
			csstransforms3d: function() {
				return !!test('perspective');
			},
			csstransitions: function() {
				return !!test('transition');
			},
			cssanimations: function() {
				return !!test('animation');
			}
		};

	function test(property, prefixed) {
		var result = false,
			upper = property.charAt(0).toUpperCase() + property.slice(1);

		$.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function(i, property) {
			if (style[property] !== undefined) {
				result = prefixed ? property : true;
				return false;
			}
		});

		return result;
	}

	function prefixed(property) {
		return test(property, true);
	}

	if (tests.csstransitions()) {
		/* jshint -W053 */
		$.support.transition = new String(prefixed('transition'))
		$.support.transition.end = events.transition.end[ $.support.transition ];
	}

	if (tests.cssanimations()) {
		/* jshint -W053 */
		$.support.animation = new String(prefixed('animation'))
		$.support.animation.end = events.animation.end[ $.support.animation ];
	}

	if (tests.csstransforms()) {
		/* jshint -W053 */
		$.support.transform = new String(prefixed('transform'));
		$.support.transform3d = tests.csstransforms3d();
	}

})(window.Zepto || window.jQuery, window, document);

'use strict';

(function ($) {
  var routesApp = {
    // All pages
    common: {
      init: function () {
      }
    },
    // Home page
    home: {
      init: function () {
      // JavaScript to be fired on the home page
      var itensMenu = $('ul.main-menu > li > a');
         itensMenu.each(function(i){
             $(this).on('click', function(e) {
                 e.preventDefault();
                 var goToSection = $( $(this).attr('href') );
                 itensMenu.removeClass('active');
                 if(goToSection.attr('id') === 'home'){
                     $('body').animatescroll();
                 } else {
                     $(this).addClass('active');
                     goToSection.animatescroll();
                 }
             });
         });

         $('.owl-carousel').owlCarousel({
              loop:true,
              nav:true,
              dots: false,
              navText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:1
                  },
                  1000:{
                      items:1
                  }
              }
          })
        //scroll-top
          $(function() {
            $("#scroll-top").on('click', function() {
              $("HTML, BODY").animate({
                  scrollTop: 0
              }, 1000);
            });
          });
          //mouse
          $(function() {
            $("#scroll-clientes").on('click', function() {
              $("HTML, BODY").animate({
                  scrollTop: 300
              }, 1000);
            });
          });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var namespace = routesApp;
      funcname = (funcname === undefined) ? 'init' : funcname;
      if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      UTIL.fire('common');

      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
      });
    }
  };

  $(document).ready(UTIL.loadEvents);
})(jQuery);