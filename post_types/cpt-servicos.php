<?php

use PostTypes\PostType;

$servico = new PostType(array(
    'name' => 'services',
    'singular' => 'Servico',
    'plural' => 'Servicos',
    'slug' => 'Servico'
), array('supports' => array('title', 'editor', 'thumbnail'), 'taxonomies'  => array( 'category' )), array(
    'parent_item_colon'     => 'Item anterior:',
    'all_items'             => 'Todos os Servicos',
    'add_new_item'          => 'Adicionar novo servico',
    'add_new'               => 'Adicionar novo',
    'new_item'              => 'Novo servico',
    'edit_item'             => 'Editar servico',
    'update_item'           => 'Atualizar servico',
    'view_item'             => 'Visualizar servico',
    'view_items'            => 'Visualizar servicos',
    'search_items'          => 'Procurar servico',
    'not_found'             => 'Nenhum servico encontrado',
    'not_found_in_trash'    => 'Nenhum servico encontrado na lixeira',
    'featured_image'        => 'Imagem de destaque',
    'set_featured_image'    => 'Definir imagem de destaque',
    'remove_featured_image' => 'Remover imagem em destaque',
    'use_featured_image'    => 'Usar como imagem de destaque',
    'insert_into_item'      => 'Inserir em coluna',
    'uploaded_to_this_item' => 'Enviar para este servico',
    'items_list'            => 'Lista de servico',
    'items_list_navigation' => 'Navegar por servicos da lista',
    'filter_items_list'     => 'Filtrar lista de servico',
));

$servico->flush();
