<?php

use PostTypes\PostType;

$time = new PostType(array(
    'name' => 'time',
    'singular' => 'Time',
    'plural' => 'Times',
    'slug' => 'Time'
), array('supports' => array('title', 'editor', 'thumbnail'), 'taxonomies'  => array( 'category' )), array(
    'parent_item_colon'     => 'Item anterior:',
    'all_items'             => 'Todos os times',
    'add_new_item'          => 'Adicionar novo time',
    'add_new'               => 'Adicionar novo',
    'new_item'              => 'Novo time',
    'edit_item'             => 'Editar time',
    'update_item'           => 'Atualizar time',
    'view_item'             => 'Visualizar time',
    'view_items'            => 'Visualizar time',
    'search_items'          => 'Procurar time',
    'not_found'             => 'Nenhum servico encontrado',
    'not_found_in_trash'    => 'Nenhum servico encontrado na lixeira',
    'featured_image'        => 'Imagem de destaque',
    'set_featured_image'    => 'Definir imagem de destaque',
    'remove_featured_image' => 'Remover imagem em destaque',
    'use_featured_image'    => 'Usar como imagem de destaque',
    'insert_into_item'      => 'Inserir em coluna',
    'uploaded_to_this_item' => 'Enviar para este time',
    'items_list'            => 'Lista de time',
    'items_list_navigation' => 'Navegar por servicos da lista',
    'filter_items_list'     => 'Filtrar lista de time',
));

$time->flush();
