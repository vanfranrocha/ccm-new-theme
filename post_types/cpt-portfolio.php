<?php

use PostTypes\PostType;

$portfolio = new PostType(array(
    'name' => 'portfolio',
    'singular' => 'Portfolio',
    'plural' => 'Portfolios',
    'slug' => 'Portfolio'
), array('supports' => array('title', 'editor', 'thumbnail'), 'taxonomies'  => array( 'category' )), array(
    'parent_item_colon'     => 'Item anterior:',
    'all_items'             => 'Todos os Portfolios',
    'add_new_item'          => 'Adicionar novo portfolio',
    'add_new'               => 'Adicionar novo',
    'new_item'              => 'Novo portfolio',
    'edit_item'             => 'Editar portfolio',
    'update_item'           => 'Atualizar portfolio',
    'view_item'             => 'Visualizar portfolio',
    'view_items'            => 'Visualizar portfolio',
    'search_items'          => 'Procurar portfolio',
    'not_found'             => 'Nenhum portfolio encontrado',
    'not_found_in_trash'    => 'Nenhum portfolio encontrado na lixeira',
    'featured_image'        => 'Imagem de destaque',
    'set_featured_image'    => 'Definir imagem de destaque',
    'remove_featured_image' => 'Remover imagem em destaque',
    'use_featured_image'    => 'Usar como imagem de destaque',
    'insert_into_item'      => 'Inserir em coluna',
    'uploaded_to_this_item' => 'Enviar para este servico',
    'items_list'            => 'Lista de servico',
    'items_list_navigation' => 'Navegar por portfolio da lista',
    'filter_items_list'     => 'Filtrar lista de portfolio',
));

$portfolio->flush();
