<?php

use PostTypes\PostType;

$servico = new PostType(array(
    'name' => 'cases',
    'singular' => 'Case',
    'plural' => 'Cases',
    'slug' => 'case'
), array('supports' => array('title', 'editor', 'thumbnail'), 'taxonomies'  => array( 'category' )), array(
    'parent_item_colon'     => 'Item anterior:',
    'all_items'             => 'Todos os Cases',
    'add_new_item'          => 'Adicionar novo Case',
    'add_new'               => 'Adicionar novo',
    'new_item'              => 'Novo servico',
    'edit_item'             => 'Editar servico',
    'update_item'           => 'Atualizar Case',
    'view_item'             => 'Visualizar Case',
    'view_items'            => 'Visualizar Case',
    'search_items'          => 'Procurar Case',
    'not_found'             => 'Nenhum servico encontrado',
    'not_found_in_trash'    => 'Nenhum servico encontrado na lixeira',
    'featured_image'        => 'Imagem de destaque',
    'set_featured_image'    => 'Definir imagem de destaque',
    'remove_featured_image' => 'Remover imagem em destaque',
    'use_featured_image'    => 'Usar como imagem de destaque',
    'insert_into_item'      => 'Inserir em coluna',
    'uploaded_to_this_item' => 'Enviar para este Case',
    'items_list'            => 'Lista de Cases',
    'items_list_navigation' => 'Navegar por servicos da lista',
    'filter_items_list'     => 'Filtrar lista de servico',
));

$servico->flush();
